'''!@file       Lab0x05_taskUser.py
    @brief      Implimentation of User Interface task as an FSM
    @details    The task uses the USB VCP (Virtual COM Port) to take character input 
                from the user working at a serial terminal such as PuTTY. It uses 
                this input with Finite State Machine logic to determine which functionality
                should be executed based on that user input. This file works cooperatively
                with taskEncoder.py to allow both files' functionality to be executed quasi-
                simultaneously.
                
    @author     Brianna Roberts
    @author     Logan Williamson
    @date       01/26/2022
'''
import micropython, array, gc
from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP

##  @brief Creates object S0_INIT connected to constant 0
#   @details S0_INIT is used as an initialization state
#   it prints the help interface block and sends FSM to state 1 (S1_Wait)
S0_INIT = micropython.const(0)

##  @brief Creates object S1_Wait connected to constant 1
#   @details S1_Wait is used as an wait state. Used to decode what the user is 
#   inputting. sends user to respective states given specific keyboard inputs
S1_Wait = micropython.const(1)

##  @brief Creates object S2_zCalibrate connected to constant 2
#   @details S2_zZeroEnc1 is used as a zeroing state. If user inputs z or Z,
#   they are sent to this state which zero's the encoder from the taskEncoder
#   uses zFlag to communicate between all files
S2_zCalibrate = micropython.const(2)

##  @brief Creates object S3_pPrintPos connected to constant 3
#   @details S3_pPrintPos is used as a printing state. If user inputs p or P,
#   they are sent to this state which prints ONE value of the current position 
#   from the encoder. uses POS to connect between files
S3_pPrintPos = micropython.const(3)

##  @brief Creates object S5_vVelocity connected to constant 5
#   @details S5_vVelocity is used as a velocity state. If user inputs v or V,
#   they are sent to this state which prints ONE value of the current velocity value
#   from the encoder. uses speedDATA to connect between files
S4_vVelocity = micropython.const(4)

##  @brief Creates object S6_mDutyCtrl connected to constant 6
#   @details S6_mDutyCtrl is used to set any and all system parameters which the 
#   user may need to control. If user inputs m, M, t, k, or y, they are sent to this 
#   state. In this state, the user can input positive or negative numbers, including
#   decimal numbers, for the duty cycle, setpoint, closed-loop gain(s) for various control
#   states related to the operation of the motors.
S6_mDutyCtrl = micropython.const(6)

##  @brief Creates object S7_clrFault connected to constant 7
#   @details S7_clrFault is used to check if there is a fault detected by the
#   driver. Uses flag variable with boolean logic. flag used is faultFlag
S7_clrFault = micropython.const(7)

##  @brief Creates object S8_gCollectThirtySec connected to constant 8
#   @details S8_gCollectThirtySec is used to collect data from encoder
#   continuously for 30 seconds. The values are then printed out all together
#   in an array. If the user decides to end data collection early, they may
#   do this by entering s or S on the keyboard.
S8_gCollectThirtySec = micropython.const(8)

##  @brief Creates object S9_PrintData connected to constant 9
#   @details S9_PrintData is used to print the compiled data that has been 
#   collected from both Encoder data or Testing data. 
S9_PrintData = micropython.const(9)

##  @brief Creates object S10_testingDataCrunch connected to constant 10
#   @details S10_testingDataCrunch is the state used to collect a sample of velocity
#   data points and use them to compute the average velocity of that set. This duty cycle
#   is then written to posArray and the averaged velocity value is written to velArray 
#   before being used to print a dataset representing speed as a function of duty cycle.
S10_testingDataCrunch = micropython.const(10)

##  @brief Creates object S11_wToggleCTRmode connected to constant 11
#   @details S11_wToggleCTRmode state either enables or disables the closed
#   loop control. If it is disabled, the velocity set will be off proportionally 
#   to the duty cycle. When it is enabled, the corrected gain will alter the 
#   duty cycle to be more accurate. 
S11_wToggleCTRmode = micropython.const(11)

##  @brief Creates object S12_rStepResponse connected to constant 12
#   @details S12_rStepResponse records the step response of the motor when
#   the user has put the motor in closed-loop control mode and 
S12_rStepResponse = micropython.const(12)

def taskUserFcn(taskName, period, zFlag, timDATA, posDATA, calDATA, DATA, speedDATA, faultFlag, duty_y, duty_x, K_p, K_d, setpoint_x,setpoint_y, wFlag):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Shared flag variable which causes FSM to enter zeroing state.
        @param timDATA      Shared data for the time during a data recording session. This is not currently utilized by 
                            taskUser.py (instead, it is immediately fed into DATA); however, this is shared to taskUser
                            for debugging purposes.
        @param posDATA      Shared data for printing the position of the encoder upon 'p' or 'P' press.
        @param delDATA      Shared data for printing the delta value of the encoder upon 'd' or 'D' press.
        @param DATA         Shared data for data recording upon 'g' or 'G' press.     
        @param speedDATA    Shared data for printing the velocity of the encoder upon 't' or 'T' press.
        @param faultFlag    Shared flag variable for indicating when a fault condition has occurred.
        @param duty1        Shared data for controlling the duty cycle of the motor_1 object, effectively controlling its speed.
        @param duty2        Shared data for controlling the duty cycle of the motor_2 object, effectively controlling its speed.
        @param K_p          Shared data for controlling the gain for closed-loop motor control
        @param setpoint     Shared data corresponding to the reference or desired velocity of the motor for closed-loop control.\
        @param wFlag        Shared flag variable set or reset by the user entering 'w' or 'W' in State 1. This variable is shared 
                            with taskMotor and will determine whether taskMotor is in the open-loop or closed-loop control state.
        @param kFlag        Local flag variable used to reserve the character buffer for user to set the closed-loop gain.
    '''
    
    ##  @brief S1_Wait is the holding state that keeps the program open and checking
    #   for user input.
    #   @details This object represents the first state in our Finite State Machine.
    #   In this state, the taskUser checks the serial port object for any waiting
    #   user input characters. This state contains the logic which can transition the
    #   FSM to other states if a valid user input character is entered.
    state = S0_INIT
    
    ##  @brief start_time which starts the clock.
    #   @details A timestamp, in microseconds, indicating the current time for 
    #   the run.
    start_time = ticks_us()
    
    ##  @brief next_time sets an appointment time for the next run.
    #   @details A timestamp, in microseconds, indicating when the next iteration of the
    #   generator must run.
    next_time = ticks_add(start_time, period)
    
    ##  @brief serport is a (virtual) serial port object.
    #   @details A (virtual) serial port object used for reading character inputs
    #   during the cooperative operation of our taskUser state machine with its taskEncoder
    #   counterpart.
    serport = USB_VCP()
    
    ##  @brief numItems tracks the number of data points collected in State 5.
    #   @details This object represents the number of data points that have been passed
    #   to taskUser from taskEncoder while taskUser is in the data collection state,
    #   S5_gCollectThirtySec. This object is incremented as each data point is passed in
    #   until it is equal to the desired number of data points defined by totItems.
    numItems = 0
    
    ##  @brief totItems determines how many data points to gather during collection.
    #   @details This object represents the maximum value that numItems can increment
    #   to as data is being recorded in State 5, S5_gCollectThirtySec. This corresponds
    #   to the hundredths of a second; in other words, totItems = 3001 will dictate a 
    #   data collection period of 30 seconds, with one data point every 0.01 seconds.
    totItems = 1001

    ### May want these back for plot generation in the future
#     ##  @brief numPrint tracks the number of data points printed in State 6.
#     #   @details This object represents the number of data points that have been
#     #   printed while in State 6, S6_PrintData. Once this value reaches the value
#     #   of numItems, all the data collected has been printed and the FSM transitions
#     #   back to State 1, S1_Wait. This condition also resets numItems and clears the
#     #   timeArray and posArray in preparation for the next instance of data collection.
#     numPrint = 0
    
# # Motor testing index control variables
#     ##  @brief testItems determines how many data points to gather and average during testing.
#     #   @details This object represents the maximum value that numItems can increment
#     #   to as data is being recorded in State 9, S9_tTestInt. This corresponds
#     #   to the hundredths of a second; in other words, testItems = 50 will dictate a 
#     #   data collection period of 0.2 seconds, with one data point every 0.01 seconds.
#     testItems = 100
    
    ##  @brief numItemsTestPrint tracks the number of data points collected in State 9.
    #   @details This object represents the number of data points that have been passed
    #   to taskUser from taskEncoder while taskUser is in the testing state,
    #   S9_tTestInt. This object is incremented as each duty cycle and average velocity
    #   data point is passed in until the user exits the testing interface. At this point,
    #   numItemsTest is reset to zero in preparation for future testing sessions.
    numPrintTest = 0
    
    ##  @brief dutyLim controls the magnitude of the possible closed-loop control 
    #   setpoint values availabe to the user.
    #   @details dutyLim serves to control the magnitude of the maximum and minimum setpoint speed, 
    #   in radians per second, that is available for the user to input for closed-loop control. If the user
    #   inputs a setpoint value that is greater than or equal to dutyLim, their input will be reset to
    #   the dutyLim value to maintain closed-loop controller functionality. If the user inputs a 
    #   setpoint value that is less than or equal to the negative of the dutyLim value, the setpoint 
    #   input will similarly be reset to the negative of the dutyLim value.
    dutyLim = 40
    
    ##  @brief setpointLim controls the magnitude of the possible closed-loop control 
    #   setpoint values availabe to the user.
    #   @details setpointLim serves to control the magnitude of the maximum and minimum setpoint speed, 
    #   in radians per second, that is available for the user to input for closed-loop control. If the user
    #   inputs a setpoint value that is greater than or equal to setpointLim, their input will be reset to
    #   the setpointLim value to maintain closed-loop controller functionality. If the user inputs a 
    #   setpoint value that is less than or equal to the negative of the setpointLim value, the setpoint 
    #   input will similarly be reset to the negative of the setpointLim value.
    setpointLim = 15
    
    ##  @brief Kp_lowerLim sets a lower limit on acceptable user input values for the closed-loop control gain.
    #   @details Kp_lowerLim serves to set the lower bound for gain values that will work with the closed-loop 
    #   controller contained in the closedLoop.py class and instantiated in the taskMotor.py task file. Any 
    #   user input for the controller gain that is less than or equal to this lower limit will be replaced with 
    #   this lower limit value to maintain functionality of the closed-loop controller.
    Kp_lowerLim = 0.01
    
    ##  @brief Kp_upperLim sets a lower limit on acceptable user input values for the closed-loop control gain.
    #   @details Kp_upperLim serves to set the upper bound for gain values that will work with the closed-loop 
    #   controller contained in the closedLoop.py class and instantiated in the taskMotor.py task file. Any 
    #   user input for the controller gain that is greater than or equal to this upper limit will be replaced with 
    #   this upper limit value to maintain functionality of the closed-loop controller.
    Kp_upperLim = 30
    
# Data storage arrays for testing and printing states
    ##  @brief posArray is the array in which collected encoder position data is stored.
    #   @details This object is an array which will store the encoder position 
    #   data generated during data collection in State 5, S5_gCollectThirtySec.
    #   This data is indexed from the shared parameter, DATA.
    posArray = array.array('f',totItems*[0])
    gc.collect()
    ##  @brief timeArray is the array in which collected time data is stored.
    #   @details This object is an array which will store the time data generated 
    #   during data collection in State 5, S5_gCollectThirtySec. This data is indexed
    #   from the shared parameter, DATA.
    timeArray = array.array('f',posArray)
    gc.collect()
    
    ##  @brief velArray is the array in which collected motor velocity data is stored.
    #   @details This object is an array which will store the velocity data generated during
    #   data collection in State 5, S5_gCollectThirtySec. This data is indexed from the shared
    #   parameter, DATA.
    velArray = array.array('f',posArray)
    gc.collect()

    ##  @brief Creating local mFlag variable used to simplify state diagram.
    #   @details mFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #   cycle. When this flag is raised, it will change duty cycle of motor 1.
    mFlag = False
    
    ##  @brief Creating local MFlag variable used to simplify state diagram.
    #   @details MFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #   cycle. When this flag is raised, it will change duty cycle of motor 1.
    MFlag = False
    
    ##  @brief Creating local kFlag variable used to indicate that closed-loop controller 
    #   gain is being set.
    #   @details The kFlag variable is NOT a shares variable. It acts to reserve the 
    #   character buffer for inputs related to setting the closed-loop controller 
    #   gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    kFlag = False
    
    ##  @brief Creating local dFlag variable used to indicate that closed-loop controller 
    #   derivative gain is being set.
    #   @details The dFlag variable is NOT a shares variable. It acts to reserve the 
    #   character buffer for inputs related to setting the closed-loop controller derivative
    #   gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    dFlag = False
      
    ##  @brief Creating local xFlag variable used to indicate that closed-loop controller 
    #   setpoint is being set.
    #   @details The xFlag variable is NOT a shares variable. It acts to reserve the 
    #   character buffer for inputs related to setting the closed-loop controller 
    #   gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    xFlag = False  
    
    ##  @brief Creating local yFlag variable used to indicate that closed-loop controller 
    #   setpoint is being set.
    #   @details The yFlag variable is NOT a shares variable. It acts to reserve the 
    #   character buffer for inputs related to setting the closed-loop controller 
    #   gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    yFlag = False

    ### Might want to reallocate as IMU_DataFlag for printing in the future
    # ##  @brief Creating encDataFlag variable used to simplify state diagram.
    # #   @details encDataFlag is NOT a shared variable. It is used to communicate 
    # #   between S1_Wait and  S9_PrintData.  This flag causes print state to print 
    # #   encoder data from S8_gCollectThirtySec after user manually quits using
    # #   s or S or after 30 seconds of data collection is up. 
    # encDataFlag = False
    
    # ##  @brief Create testDataFlag variable used to simplify state diagram.
    # #   @details testDataFlag is NOT a shared variable. It is used to communicate 
    # #   between S1_Wait and  S9_PrintData.  This flag causes print state to print 
    # #   Duty data from testDataFlag after user manually quits using
    # #   s or S or after 30 seconds of data collection is up. 
    # testDataFlag = False
    
    ##  @brief String for buffering of characters in the serial port of the STM32 Nucleo.
    #   @details numstr serves to act as a buffer into which characters are written when
    #   when the user is in one of the many parameter input states. This buffer maintains 
    #   characters until the user is finished with an input (presses enter) to allow for 
    #   cooperatively multitasking with taskEncoder and taskMotor to continue running in 
    #   the background.
    numstr = ''
    
    # The finite state machine must run indefinitely.
    while True:
        ##  @brief current_time tracks the time in microseconds.
        #   @details This object tracks the elapsed time since taskUser was first called
        #   in order to manage task sharing between taskUser and taskEncoder. It is used
        #   in conjunction with the designated period parameter to set 'appointments' for 
        #   the next run of taskUser and prevent it from dominating task sharing.
        current_time = ticks_us()
        
        # FSM only needs to run if interval has elapsed. must use ticks_diff() b/c of overflow
        if ticks_diff(current_time,next_time) >= 0:
        
            # reset next_time to appointment for next iteration; need ticks_add for overflow 
            next_time = ticks_add(next_time, period)
            
#state 0 code - Initialization state and printing of User Interface directions

            if state == S0_INIT:
                # On the entry to state 0 we can print a message for the user describing the
                # UI.
                print("+----------+------------------------------------------------------+")
                print('| Command  | Description                                          |')
                print("+----------+------------------------------------------------------+")
                print('|  p or P  |  Print the XYZ angular position of the platform      |')
                print('|  v or V  |  Print the XYZ angular velocity of the platform      |')
                print("+----------+------------------------------------------------------+")
                print('|    m     |  Enter specified duty cycle for motor 1              |')
                print('|    M     |  Enter specified duty cycle for motor 2              |')
                print("+----------+------------------------------------------------------+")
                print('|  w or W  |  Enable or disable closed-loop control               |')
                print('|  k or K  |  Enter new closed-loop control gain                  |')
                print('|  d or D  |  Enter new derivative closed-loop control gain       |')
                print('|  y or Y  |  Choose a theta_y position setpoint                  |')
                print('|  x or X  |  Choose a theta_x position setpoint                  |')
                print("+----------+------------------------------------------------------+")
                print('|  h or H  |  Print this help message                             |')
                print('|  Ctrl-C  |  Terminate Program                                   |')
                print("+----------+------------------------------------------------------+")
                serport.write('\r')
                if zFlag.read() == True:
                    print('Starting manual calibration. Rotate the platform until all statuses are (3)')
                    state = S2_zCalibrate   # Force calibration of IMU on startup if there is no .txt file for taskIMU to read
                else:
                    print('Calibration coefficients file detected. Bypassing manual calibration.')
                    state = S1_Wait
                
# State 1 code - Base state for awaiting user input
            elif state == S1_Wait:
                # If a character is waiting, read it
                if serport.any():
                    ##  @brief charIn is the variable where input characters are stored after reading
                    #   and decoding them.
                    #   @details Reads one character and decodes it into a string. This
                    #   decoding converts the bytes object to a standard Python string object.
                    charIn = serport.read(1).decode()
                        
                    if charIn in {'p', 'P'}:
                        print('Recieving Position Value')
                        state = S3_pPrintPos # transition to state 3
                        
                    elif charIn in {'v', 'V'}:
                        print('Recieving Velocity Value')
                        state = S4_vVelocity # transition to state 5
                    
                    elif charIn == 'm':
                        numstr = ''
                        if wFlag.read() == False:
                            print(f'Please Enter a Duty Cycle between -{dutyLim} and {dutyLim}')
                            mFlag = True
                            state = S6_mDutyCtrl
                        else:
                            print('Must be in open-loop control mode to set duty cycle manually')
                            pass
                        
                    elif charIn == 'M':
                        numstr = ''
                        if wFlag.read() == False:
                            print(f'Please enter a duty cycle between -{dutyLim} and {dutyLim}')
                            MFlag = True
                            state = S6_mDutyCtrl
                        else:
                            print('Must be in open-loop control mode to set duty cycle manually')
                            pass
                        
                    # elif charIn in {'g', 'G'}:
                    #     print('Collecting data for 30 seconds')
                    #     encDataFlag = True #IMU_DataFlag maybe?
                    #     state = S8_gCollectThirtySec
                        
                    elif charIn in {'w','W'}:
                        state = S11_wToggleCTRmode

                    elif charIn in {'k','K'}:
                        numstr = ''
                        kFlag = True
                        print(f'Please input a closed-loop controller gain (K_p) between {Kp_lowerLim} and {Kp_upperLim}:')
                        state = S6_mDutyCtrl
                        
                    elif charIn in {'d','D'}:
                        numstr = ''
                        dFlag = True
                        print(f'Please input a closed-loop controller gain (K_d) between {Kp_lowerLim} and {Kp_upperLim}:')
                        state = S6_mDutyCtrl
                        
                    elif charIn in {'y','Y'}:
                        numstr = ''
                        print(f'Enter a new closed-loop controller desired theta_y value between -{setpointLim} and {setpointLim} [°]')
                        yFlag = True
                        state = S6_mDutyCtrl
                        
                    elif charIn in {'x','X'}:
                        numstr = ''
                        print(f'Enter a new closed-loop controller desired theta_x value between -{setpointLim} and {setpointLim}[°]')
                        xFlag = True
                        state = S6_mDutyCtrl
                    
                    elif charIn in {'h', 'H'}:
                        print("+----------+------------------------------------------------------+")
                        print('| Command  | Description                                          |')
                        print("+----------+------------------------------------------------------+")
                        print('|  p or P  |  Print the XYZ angular position of the platform      |')
                        print('|  v or V  |  Print the XYZ angular velocity of the platform      |')
                        print("+----------+------------------------------------------------------+")
                        print('|    m     |  Enter specified duty cycle for motor 1              |')
                        print('|    M     |  Enter specified duty cycle for motor 2              |')
                        print("+----------+------------------------------------------------------+")
                        print('|  w or W  |  Enable or disable closed-loop control               |')
                        print('|  k or K  |  Enter new closed-loop control gain                  |')
                        print('|  d or D  |  Enter new derivative closed-loop control gain       |')
                        print('|  y or Y  |  Choose a theta_y position setpoint                  |')
                        print('|  x or X  |  Choose a theta_x position setpoint                  |')
                        print("+----------+------------------------------------------------------+")
                        print('|  h or H  |  Print this help message                             |')
                        print('|  Ctrl-C  |  Terminate Program                                   |')
                        print("+----------+------------------------------------------------------+")
                        state = S1_Wait
                        
                    elif charIn[0] == 3: # checks for ctrl-c
                        print('Terminating program')
                    else:
                        print(f'You typed invalid {charIn} from state {state}.')
            
# State 2 code - Calibrate IMU state
            elif state == S2_zCalibrate:
                print('mag_stat:',calDATA.read()[0],'acc_stat:',calDATA.read()[1],'gyr_stat:',calDATA.read()[2],'sys_stat:',calDATA.read()[3])
                if not zFlag.read():
                    print('IMU CALIBRATION IS COMPLETE')
                    state = S1_Wait

# State 3 code - Position print state
            elif state == S3_pPrintPos:
                print('Theta_x:',posDATA.read()[0],'Theta_y:',posDATA.read()[1],'Theta_z:',posDATA.read()[2]) # prints position value
                state = S1_Wait

# State 4 code - Instantaneous velocity print state
            elif state == S4_vVelocity:
                print('Omega_x:',speedDATA.read()[0],'Omega_y:',speedDATA.read()[1],'Omega_z:', speedDATA.read()[2]) # prints velocity value
                state = S1_Wait

# State 6 code - Set motor duty cycle(s) state
        # Wait for character inputs and append them to buffer string
            elif state == S6_mDutyCtrl:
                if faultFlag.read():
                    print('Fault Detected! Erroneous last data point deleted.')
                    print('Fault cleared')
                    print(f'Please input a duty cycle between -{dutyLim} and {dutyLim} to test')
                    numPrintTest-=1
                    state = S7_clrFault
                if serport.any():
                    charDuty = serport.read(1).decode()
                    if charDuty.isdigit():
                        numstr += charDuty # everytime charIn is looped appends to string
                        serport.write(charDuty)
                    elif charDuty == '-':
                        if len(numstr) == 0:
                            numstr += charDuty
                            serport.write(charDuty)
                        else:
                            pass
                    elif charDuty in {'\b', '\x08', '\x7f'}: # for backspace
                        if len(numstr)>0:
                            numstr = numstr[:-1]
                            serport.write(charDuty)
                        else:
                            pass
                    elif charDuty == ' ':
                        pass
                    elif charDuty == '.':
                        if numstr.count('.') == 0:
                            numstr += charDuty
                            serport.write(charDuty)
                        else:
                            pass
                    # elif charDuty in {'s','S'}:
                    #     if testDataFlag:
                    #         print('---------------Data Collection Results---------------')
                    #         print('          Duty Cycle [%],Velocity [rad/s]            ')
                    #     state = S9_PrintData
                    elif charDuty in {'\r', '\n'}:
                    # Set duty cycle for open loop control
                        serport.write('\r')
                        if mFlag | MFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= dutyLim:
                                    numstr = dutyLim
                                elif float(numstr) <= -dutyLim:
                                    numstr = -dutyLim
                                numstr = round(float(numstr),2)
                                if mFlag == True:
                                    duty_y.write(numstr)
                                    print('Duty Cycle for Motor 1 has been set to', duty_y.read())
                                    mFlag = False
                                    yield duty_y
                                    state = S1_Wait
                                
                                elif MFlag == True:
                                    duty_x.write(numstr)
                                    print('Duty Cycle for Motor 2 has been set to', duty_x.read())
                                    MFlag = False
                                    yield duty_x
                                    state = S1_Wait
                                    
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between -{dutyLim} and {dutyLim}, then press Enter')
                    
                    # Set gain for CL control
                        elif kFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= Kp_upperLim:
                                    numstr = Kp_upperLim
                                elif float(numstr) <= Kp_lowerLim:
                                    numstr = Kp_lowerLim
                                numstr = round(float(numstr),2)
                                kFlag = False
                                K_p.write(numstr)
                                print('Closed-loop controller gain has been set to', K_p.read())
                                yield K_p
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between {Kp_lowerLim} and {Kp_upperLim}, then press Enter')
                                state = S6_mDutyCtrl
                                
                    # Set gain for CL derivative control
                        elif dFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= Kp_upperLim:
                                    numstr = Kp_upperLim
                                elif float(numstr) <= Kp_lowerLim:
                                    numstr = Kp_lowerLim
                                numstr = round(float(numstr),2)
                                dFlag = False
                                K_d.write(numstr)
                                serport.write('\r')
                                print('Closed-loop controller derivative gain has been set to', K_d.read())
                                yield K_d
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between {Kp_lowerLim} and {Kp_upperLim}, then press Enter')
                                state = S6_mDutyCtrl
                                
                    # Set setpoint for CL control
                        elif yFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= setpointLim:
                                    numstr = setpointLim
                                elif float(numstr) <= -setpointLim:
                                    numstr = -setpointLim
                                numstr = round(float(numstr),2)
                                setpoint_y.write(numstr)
                                yield setpoint_y
                                yFlag = False
                                serport.write('\r')
                                print('Closed-loop controller setpoint has been set to', setpoint_y.read())
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between -{setpointLim} and {setpointLim}, then press Enter')
                                state = S6_mDutyCtrl
                        elif xFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= setpointLim:
                                    numstr = setpointLim
                                elif float(numstr) <= -setpointLim:
                                    numstr = -setpointLim
                                numstr = round(float(numstr),2)
                                setpoint_x.write(numstr)
                                yield setpoint_x
                                xFlag = False
                                serport.write('\r')
                                print('Closed-loop controller setpoint has been set to', setpoint_x.read())
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between -{setpointLim} and {setpointLim}, then press Enter')
                                state = S6_mDutyCtrl
            
# State 8 code - Data Collection State
            elif state == S8_gCollectThirtySec:             
                if numItems < totItems:
                   timeArray[numItems], posArray[numItems], velArray[numItems] = DATA.read()
                   numItems += 1
                   if serport.any():
                       charIn = serport.read(1).decode()
                       if charIn in {'s', 'S'}:
                           print('Ending data collection Early')
                           print('---------------Data Collection Results---------------')
                           print('      Time [s],Position [rad],Velocity [rad/s]       ')
                           state = S9_PrintData # transition to state 11
                if numItems == totItems:
                    print('---------------Data Collection Results---------------')
                    print('     Time [s],Position [rad],Velocity [rad/s]        ')
                    state = S9_PrintData
                
# # State 9 code - Printing State
#             # Printing for data collection ('g' press) state
#             elif state == S9_PrintData:
#                 if encDataFlag:
#                     if numPrint<numItems:
#                         print(f'        {((timeArray[numPrint]-timeArray[0])/1000):.2f}  ,   {(posArray[numPrint]):.2f}     ,   {(velArray[numPrint]):.2f}')
#                         numPrint += 1
#                     elif numPrint >= numItems:
#                         numPrint = 0
#                         numItems = 0
#                         encDataFlag = False
#                         state = S1_Wait
                        
#                 # Printing for testing ('t' press) state
#                 elif testDataFlag:
#                     if numPrint<numPrintTest:
#                         print(f'              {(posArray[numPrint]):.2f}     ,     {(velArray[numPrint]):.2f}')
#                         numPrint+=1
#                     elif numPrint == numPrintTest:
#                         numItems = 0
#                         numPrint = 0
#                         numPrintTest = 0
#                         testDataFlag = False
#                         state = S1_Wait
                
#                 elif SRFlag:
#                     if numPrint<numItems:
#                         print(f'        {((timeArray[numPrint]-timeArray[0])/1000):.2f}  ,    {(velArray[numPrint]):.2f}    ,   {(posArray[numPrint]):.2f}')
#                         numPrint += 1
#                     elif numPrint >= numItems:
#                         numPrint = 0
#                         numItems = 0
#                         SRFlag = False
#                         state = S1_Wait
                    
            elif state == S11_wToggleCTRmode:
                if wFlag.read() == False:
                    print('Closed Loop Control Enabled')
                    wFlag.write(True)
                    yield wFlag
                    state = S1_Wait
                elif wFlag.read() == True:
                    wFlag.write(False)
                    print('Closed Loop Control Disabled')
                    yield wFlag
                    state = S1_Wait               
                    
                # if kFlag == True:
                #     print(f'Please input a closed-loop controller gain (K_p) between {Kp_lowerLim} and {Kp_upperLim}:')
                #     numstr = ''
                #     state = S6_mDutyCtrl
                # elif yFlag == True:
                #     print(f'Enter a new closed-loop controller setpoint value between -{setpointLim} and {setpointLim} [rad/s]')
                #     numstr = ''
                #     state = S6_mDutyCtrl
                    

            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            yield state
        else:
            yield None