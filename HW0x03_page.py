'''!@file               HW0x03_page.py
    @brief              HW0x03_page.py documentation page.
    @details            This page documents the HW0x03_page.py file,which generates a simulation for the ball and platform calculations.

	@page page5			HW0x03_page.py
                        *Please see the files tab for file documentation
                        
    @section sec_intro5 Introduction            
    
	@section sec_TSK5	Homework 3 Jacobian Calucations

    <a href="https://drive.google.com/file/d/1SfDhpG8Y2ITn4n5ZmkNhpW0Qnr4vOMcz/view?usp=sharing">https://drive.google.com/file/d/1SfDhpG8Y2ITn4n5ZmkNhpW0Qnr4vOMcz/view?usp=sharing</a>
    @author              Brianna Roberts
    @author              Logan Williamson
    @date                February 21, 2022
'''