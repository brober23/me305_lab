'''!@file       Lab0x02_taskUser.py
    @brief      Implimentation of User Interface task as an FSM.
    @details    The task uses the USB VCP (Virtual COM Port) to take character input 
                from the user working at a serial terminal such as PuTTY. It uses 
                this input with Finite State Machine logic to determine which functionality
                should be executed based on that user input. This file works cooperatively
                with taskEncoder.py to allow both files' functionality to be executed quasi-
                simultaneously.
                
    @author     Brianna Roberts
    @author     Logan Williamson
    @date       01/26/2022
'''
import micropython, array
from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP
# import speedDATA

##  @brief creates object S0_INIT connected to constant 0
#   @details S0_INIT is used as an initialization state
#   it prints the help interface block and sends FSM to state 1 (S1_Wait)
S0_INIT = micropython.const(0)

##  @brief creates object S1_Wait connected to constant 1
#   @details S1_Wait is used as an wait state. Used to decode what the user is 
#   inputting. sends user to respective states given specific keyboard inputs
S1_Wait = micropython.const(1)

##  @brief creates object S2_zZeroEnc1 connected to constant 2
#   @details S2_zZeroEnc1 is used as a zeroing state. If user inputs z or Z,
#   they are sent to this state which zero's the encoder from the taskEncoder
#   uses zFlag to communicate between all files
S2_zZeroEnc1 = micropython.const(2)

##  @brief creates object S3_pPrintPos connected to constant 3
#   @details S3_pPrintPos is used as a printing state. If user inputs p or P,
#   they are sent to this state which prints ONE value of the current position 
#   from the encoder. uses POS to connect between files
S3_pPrintPos = micropython.const(3)

##  @brief creates object S4_dDelta connected to constant 4
#   @details S4_dDelta is used as a delta state. If user inputs d or D,
#   they are sent to this state which prints ONE value of the current delta value
#   from the encoder. uses DEL to connect between files
S4_dDelta = micropython.const(4)

##  @brief creates object S5_vVelocity connected to constant 5
#   @details S5_vVelocity is used as a velocity state. If user inputs v or V,
#   they are sent to this state which prints ONE value of the current velocity value
#   from the encoder. uses speedDATA to connect between files
S5_vVelocity = micropython.const(5)

##  @brief creates object S6_mDutyCtrl connected to constant 6
#   @details S6_mDutyCtrl is used to set the duty cycle of motor 1. 
#   If user inputs m, they are sent to this state which prompts the user to 
#   input their desired duty cycle. Uses variable duty1 to connect between files
S6_mDutyCtrl = micropython.const(6)

##  @brief creates object S8_clrFault connected to constant 8
#   @details S8_clrFault is used to check if there is a fault detected by the
#   driver. Uses flag variable with boolean logic. flag used is faultFlag
S7_clrFault = micropython.const(7)

##  @brief creates object S8_gCollectThirtySec connected to constant 9
#   @details S8_gCollectThirtySec is used to collect data from encoder
#   continuously for 30 seconds. The values are then printed out all together
#   in an array. If the user decides to end data collection early, they may
#   do this by entering s or S on the keyboard.
S8_gCollectThirtySec = micropython.const(8)

##  @brief creates object S9_tTestInt connected to constant 9
#   @details S9_tTestInt is used to repeatedly prompt the user for duty cycles 
#   for motor 1.  Determines average velocity for each prompted duty cycle. 
#   these values (duty cycle, average velocity) are then printed out in array
#   once user is done testing, in which they type s or S on the keyboard.
S9_tTestInt = micropython.const(8)

##  @brief creates object S10_PrintData connected to constant 10
#   @details S10_PrintData is used to print the compiled data that has been 
#   collected from both Encoder data or Testing data. 
S10_PrintData = micropython.const(10)

#need a separate share for a position and delta 


def taskUserFcn(taskName, period, zFlag, timDATA, posDATA, delDATA, DATA,speedDATA, faultFlag, duty1, duty2):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Shared flag variable which causes FSM to enter zeroing state.
        @param timDATA      Shared data for the time during a data recording session. This is not currently utilized by 
                            taskUser.py (instead, it is immediately fed into DATA); however, this is shared to taskUser
                            for debugging purposes.
        @param posDATA      Shared data for printing the position of the encoder upon 'p' or 'P' press.
        @param delDATA      Shared data for printing the delta value of the encoder upon 'd' or 'D' press.
        @param DATA         Shared data for data recording upon 'g' or 'G' press.     
    '''
    
    # The state of the FSM to run on the next iteration of the generator.
    ##  @brief S1_Wait is the holding state that keeps the program open and checking
    #   for user input.
    #   @details This object represents the first state in our Finite State Machine.
    #   In this state, the taskUser checks the serial port object for any waiting
    #   user input characters. This state contains the logic which can transition the
    #   FSM to other states if a valid user input character is entered.
    state = S0_INIT
    
    ##  @brief start_time which starts the clock.
    #   @details A timestamp, in microseconds, indicating the current time for 
    #   the run.
    start_time = ticks_us()
    
    ##  @brief next_time sets an appointment time for the next run.
    #   @details A timestamp, in microseconds, indicating when the next iteration of the
    #   generator must run.
    next_time = ticks_add(start_time, period)
    
    ##  @brief serport is a (virtual) serial port object.
    #   @details A (virtual) serial port object used for reading character inputs
    #   during the cooperative operation of our taskUser state machine with its taskEncoder
    #   counterpart.
    serport = USB_VCP()
    
    ##  @brief numItems tracks the number of data points collected in State 5.
    #   @details This object represents the number of data points that have been passed
    #   to taskUser from taskEncoder while taskUser is in the data collection state,
    #   S5_gCollectThirtySec. This object is incremented as each data point is passed in
    #   until it is equal to the desired number of data points defined by totItems.
    numItems = 0
    
    ##  @brief totItems determines how many data points to gather during collection.
    #   @details This object represents the maximum value that numItems can increment
    #   to as data is being recorded in State 5, S5_gCollectThirtySec. This corresponds
    #   to the hundredths of a second; in other words, totItems = 3001 will dictate a 
    #   data collection period of 30 seconds, with one data point every 0.01 seconds.
    totItems = 3001
    
    ##  @brief posArray is the array in which collected encoder position data is stored.
    #   @details This object is an array which will store the encoder position 
    #   data generated during data collection in State 5, S5_gCollectThirtySec.
    #   This data is indexed from the shared parameter, DATA.
    posArray = array.array('l',totItems*[0])
    
    ##  @brief timeArray is the array in which collected time data is stored.
    #   @details This object is an array which will store the time data generated 
    #   during data collection in State 5, S5_gCollectThirtySec. This data is indexed
    #   from the shared parameter, DATA.
    timeArray = array.array('l',totItems*[0])
    
    #creating empty array that will compile duty data into this
    velArray = []
    
    dutyArray = []
    
    ##  @brief numPrint tracks the number of data points printed in State 6.
    #   @details This object represents the number of data points that have been
    #   printed while in State 6, S6_PrintData. Once this value reaches the value
    #   of numItems, all the data collected has been printed and the FSM transitions
    #   back to State 1, S1_Wait. This condition also resets numItems and clears the
    #   timeArray and posArray in preparation for the next instance of data collection.
    numPrint = 0
    
    ##  @brief creating mFlag variable used to simplify state diagram.
    #   @details mFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #   cycle. When this flag is raised, it will change duty cycle of motor 1.
    mFlag = False
    
    ##  @brief creating mFlag variable used to simplify state diagram.
    #   @details mFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #   cycle. When this flag is raised, it will change duty cycle of motor 1.
    MFlag = False
    
    ##  @brief creating encDataFlag variable used to simplify state diagram.
    #   @details encDataFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S10_PrintData.  This flag causes print state to print 
    #   encoder data from S8_gCollectThirtySec after user manually quits using
    #   s or S or after 30 seconds of data collection is up. 
    encDataFlag = False
    
    ##  @brief creating testDataFlag variable used to simplify state diagram.
    #   @details testDataFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S10_PrintData.  This flag causes print state to print 
    #   Duty data from testDataFlag after user manually quits using
    #   s or S or after 30 seconds of data collection is up. 
    testDataFlag = False
    
    # The finite state machine must run indefinitely.
    while True:
        ##  @brief current_time tracks the time in microseconds.
        #   @details This object tracks the elapsed time since taskUser was first called
        #   in order to manage task sharing between taskUser and taskEncoder. It is used
        #   in conjunction with the designated period parameter to set 'appointments' for 
        #   the next run of taskUser and prevent it from dominating task sharing.
        current_time = ticks_us()
        
        # FSM only needs to run if interval has elapsed. must use ticks_diff() b/c of overflow
        if ticks_diff(current_time,next_time) >= 0:
        
            # reset next_time to appointment for next iteration; need ticks_add for overflow 
            next_time = ticks_add(next_time, period)
            
            #state 0 code
            if state == S0_INIT:
                # On the entry to state 0 we can print a message for the user describing the
                # UI.
                print("+----------------------------------------------------------------+")
                print('| Encoder Testing Interface                                      |')
                print("+----------------------------------------------------------------+")
                print('| Use the following commands:                                    |')
                print('|  z or Z    Zero the position of encoder 1                      |')
                print('|  p or P    Print the position of encoder 1                     |')
                print('|  d or D    Print the delta (speed) of encoder 1                |')
                print('|  v or V    Print out velocity of encoder 1                     |')
                print('|    m       Enter specified duty cycle for motor 1              |')
                print('|    M       Enter specified duty cycle for motor 2              |')
                print('|  c or C    Clear a fault condition triggered by the DRV 8847   |')
                print('|  g or G    Collect data from encoder 1 for 30 seconds          |')
                print('|  t or T    Start Testing Interface                             |')
                print('|  s or S    End data collection prematurely                     |')
                print('|  h or H    Print this help message                             |')
                print('|  Ctrl-C    Terminate Program                                   |')
                print("+----------------------------------------------------------------+")
                
            
                state = S1_Wait
            elif state == S1_Wait: 
                # If a character is waiting, read it
                if serport.any():
                    ##  @brief charIn is the variable where input characters are stored after reading
                    #   and decoding them.
                    #   @details Reads one character and decodes it into a string. This
                    #   decoding converts the bytes object to a standard Python string object.
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'z', 'Z'}:
                        print("Zeroing the position")
                        zFlag.write(True)
                        state = S2_zZeroEnc1 # transition to state 2
                        
                    elif charIn in {'p', 'P'}:
                        print('Recieving Position Value')
                        state = S3_pPrintPos # transition to state 3
                        
                    elif charIn in {'d', 'D'}:
                        print('Recieving Delta Value')
                        state = S4_dDelta # transition to state 4
                        
                    elif charIn in {'v', 'V'}:
                        print('Recieving Velocity Value')
                        state = S5_vVelocity # transition to state 4
                    
                    elif charIn == 'm':
                        numstr = ''
                        print("Please Enter a Duty Cycle: ")
                        mFlag = True
                        state = S6_mDutyCtrl
                        
                    elif charIn == 'M':
                        numstr = ''
                        print("Please Enter a Duty Cycle: ")
                        MFlag = True
                        state = S6_mDutyCtrl
                        
                    elif charIn in {'c', 'C'}:
                        print("Clearing Fault ")
                        faultFlag.write(True)
                        state = S7_clrFault    
                        
                    elif charIn in {'g', 'G'}:
                        print('Running encoders for 30 seconds')
                        encDataFlag = True
                        state = S8_gCollectThirtySec 
                        
                    elif charIn in {'t', 'T'}:
                        print('Entering Motor Testing Mode')
                        testDataFlag = True
                        state = S9_tTestInt 
                        
                    # might not need this charIn check
                    elif charIn in {'s', 'S'}:
                        print('Ending data collection')
                        state = S10_PrintData # transition to state 10
                        
                    elif charIn in {'h', 'H'}: 
                        
                        print("+----------------------------------------------------------------+")
                        print('| Encoder Testing Interface                                      |')
                        print("+----------------------------------------------------------------+")
                        print('| Use the following commands:                                    |')
                        print('|  z or Z    Zero the position of encoder 1                      |')
                        print('|  p or P    Print the position of encoder 1                     |')
                        print('|  d or D    Print the delta (speed) of encoder 1                |')
                        print('|  v or V    Print out velocity of encoder 1                     |')
                        print('|    m       Enter specified duty cycle for motor 1              |')
                        print('|    M       Enter specified duty cycle for motor 2              |')
                        print('|  c or C    Clear a fault condition triggered by the DRV 8847   |')
                        print('|  g or G    Collect data from encoder 1 for 30 seconds          |')
                        print('|  t or T    Start Testing Interface                             |')
                        print('|  s or S    End data collection prematurely                     |')
                        print('|  h or H    Print this help message                             |')
                        print('|  Ctrl-C    Terminate Program                                   |')
                        print("+----------------------------------------------------------------+")
                        
                    elif charIn[0] == 3: # checks for ctrl-c
                        print('Terminating program')
                    else:
                        print(f"You typed invalid {charIn} from state 0.")
            
            
            # State 2 code - Zero Encoder Position State
            elif state == S2_zZeroEnc1:
                if not zFlag.read():
                    print('zFlag is zero')
                    state = S1_Wait
                
            # State 3 code - Position Print State
            elif state == S3_pPrintPos:
                print(posDATA.read()) # prints position value       
                state = S1_Wait

            # State 4 code - Delta Print State
            elif state == S4_dDelta:
                print(delDATA.read()) # prints delta value
                state = S1_Wait
                
            elif state == S5_vVelocity:
                print(speedDATA.read()) # prints velocity value
                state = S1_Wait

            elif state == S6_mDutyCtrl:                             
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn.isdigit:
                        numstr += charIn # everytime charIn is looped appends to string
                    elif charIn == '-':
                        if len(numstr) == 0:
                            numstr += charIn
                        else:
                            pass
                    elif charIn in {'\b', '\x08'}: # for backspace
                        if len(numstr) == 0:
                            pass
                        else:
                            charIn.rip  # not sure which one to take from
                            numstr.rip 
                            
                    elif charIn == ' ':
                        pass
                    
                    elif charIn in {'\r', '\n'}: # for enter 
                        float(numstr)
                        # can also do int(numstr)
                else:
                    state = S6_mDutyCtrl
                         
                    # this allowed me to remove state 7
                    if mFlag == True:
                        print('Duty Cycle for Motor 1 has been set to numstr')
                        duty1.write(numstr)
                        mFlag = False
                        state = S1_Wait
                        
                    elif MFlag == True:
                        print('Duty Cycle for Motor 2 has been set to numstr')
                        duty2.write(numstr)
                        MFlag = False
                        state = S1_Wait

            elif state == S7_clrFault: 
                if not faultFlag.read():
                    print('Driver fault has been dealt with')
                    state = S1_Wait
                    
                else:
                    pass
            
            # State 9 code - Data Collection State
            elif state == S8_gCollectThirtySec:             
                if numItems < totItems:
                   timeArray[numItems], posArray[numItems] = DATA.read()
                   numItems += 1
                   if serport.any():
                       charIn = serport.read(1).decode()
                       if charIn in {'s', 'S'}:
                           print('Ending data collection Early')
                           state = S10_PrintData # transition to state 11
                if numItems == totItems:
                    state = S10_PrintData
                    
            elif state == S9_tTestInt:
                # do we create empty array and append to end of it every time
                # user tests additional duty cycle?
                velArray = []
                
                dutyArray = []
                #creates while loop that continues collecting duty and velocity data
                while True:
                    if serport.any():
                        numstr = ''
                        # creates a float for duty cycle
                        while True: 
                            charIn = serport.read(1).decode()
                            if charIn.isdigit:
                                numstr += charIn # everytime charIn is looped appends to string
                            elif charIn == '-':
                                if len(numstr) == 0:
                                    numstr += charIn
                                else:
                                    pass
                            elif charIn in {'\b', '\x08'}: # for backspace
                                if len(numstr) == 0:
                                    pass
                                # .rip pulls end of string out?
                                else:
                                    charIn.rip  # not sure which one to take from
                                    numstr.rip         
                            elif charIn in {'\r', '\n'}: # for enter 
                                # might also need to account for 'space'
                                    float(numstr)
                                    # can also do int(numstr)
                                    
                                    #this should break the while loop
                                    break
                                
                        duty1.write(numstr)
                        #will then need to find velocity
                        
                        #appends user input duty cycle into dutyArray
                        dutyArray.append(numstr) 
                        #append with value for velocity
                        velArray.append(speedDATA.read)
                                
                
            
            # State 10 - Printing State
            elif state == S10_PrintData:
                
                if encDataFlag:
                    if numPrint<numItems:
                        print(f'{((timeArray[numPrint]-timeArray[0])/1000):.2f},{(posArray[numPrint]):.2f}')
                        numPrint += 1
                    elif numPrint == numItems:
                        numPrint = 0
                        numItems = 0
                        posArray = array.array('l',totItems*[0])
                        timeArray = array.array('l',totItems*[0])
                        state = S1_Wait
                        
                # configure for testing state
                elif testDataFlag:
                    for i in len(dutyArray):
                        print(f'{(dutyArray[i]):.2f},{(velArray[i]):.2f}')
                        # might not need this:
                        # i += 1
                    state = S1_Wait
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            yield state
        else:
            yield None

# # The following code would usually belong in main.py but is utilized here to
# # test the task in isolation from other tasks. Eventually this will not be
# # practical once the tasks interface with eachother using shared variables.
# if __name__ == '__main__':
    
#     # A generator object created from the generator function for the user
#     # interface task.
#     task1 = taskUserFcn('T1', 10_000, zFlag)
    
#     # To facilitate running of multiple tasks it makes sense to create a task
#     # list that can be iterated through. Adding more task objects to the list
#     # will allow the tasks to run together cooperatively.
#     taskList = [task1]
    
#     # The system should run indefinitely until the user interrupts program flow
#     # by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
#     # using the next() function on each item in the task list.
#     while True:
#         try:
#             for task in taskList:
#                 next(task)
        
#         except KeyboardInterrupt:
#             break
            
#     # In addition to exiting the program and informing the user any cleanup code
#     # should go here. An example might be un-configuring certain hardware
#     # peripherals like the User LED or perhaps any callback functions.
#     print('Program Terminating')