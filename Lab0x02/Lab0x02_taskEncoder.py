"""!@file       Lab0x02_taskEncoder.py
    @brief      A method for instantiating, updating the position, and zeroing a quadrature encoder object.
    @details    This file calls the encoder.py class to generate an encoder object. It is then called from
                an external file to perform updates to the position of that encoder object, and to zero/reset
                the absolute position of that encoder object. The external file utilizes other tasks in addition;
                therefore, this file is set up to work within a cooperative multitasking context. In particular, 
                this file works with taskUser.py.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       01/23/2022
"""
# import Encoder.py class, other required libraries
import Lab0x02_encoder, pyb, micropython
from time import ticks_us, ticks_ms, ticks_add, ticks_diff

S0_INIT = micropython.const(0)
S1_Update = micropython.const(1)
S2_Zero = micropython.const(2)

def taskEncoderFcn(taskName, period, zFlag, timDATA, posDATA, delDATA, speedDATA, DATA):
    '''!@brief              A generator to implement the Encoder task as an FSM.
        @details            The task runs as a generator function and requires a 
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Shared flag variable which causes FSM to enter zeroing state.
        @param timDATA      Shared data for the time during a data recording session. This is not currently utilized by 
                            taskUser.py (instead, it is immediately fed into DATA); however, this is shared to taskUser
                            for debugging purposes.
        @param posDATA      Shared data for printing the position of the encoder upon 'p' or 'P' press.
        @param delDATA      Shared data for printing the delta value of the encoder upon 'd' or 'D' press.
        @param DATA         Shared data for data recording upon 'g' or 'G' press.             
    '''
    
    ##  @brief creates object S0_INIT connected to constant 0
    #   @details S0_INIT is used to initialize the FSM. Simply transitions from S0 
    #   to S1.
    state = S0_INIT
    
    ##  @brief Creates variable init_time to initiate the clock. 
    #   @details Establishes time value when taskEncoderFcn was first called
    #   this value will start when main is run.
    init_time = ticks_ms()
    
    ##  @brief encoder1 is the encoder object used for data collection.
    #   @details Instantiate encoder1 object and connect instantiation to pyboard.
    #   Connects to pyboard pins PB6 and PB7 and timer 4. 
    encoder1 = encoder.Encoder(pyb.Pin.board.PB6,pyb.Pin.board.PB7,4)
    
    ##  @brief Object next_time sets an appointment time for the next run.
    #   @details A timestamp, in microseconds, indicating when the next iteration of the
    #   generator must run. Uses ticks_add to add current ticks to period.
    next_time = ticks_add(ticks_us(),period)
    
    ##  @brief Object prevTime sets the time of the last taskEncoder run.
    #   @details A timestamp, in microseconds, indicating when the previous iteration 
    #   of taskEncoder ran. This is used with the currentTime and the delta value
    #   of the encoder to approximate the instantaneous speed of the encoder.
    prevTime = 0
    
    while True:
        ##  @brief Object currentTime sets the time clock for this task.
        #   @details A timestamp, in microseconds, indicating the current clock time
        #   for this class. 
        currentTime = ticks_us()
        # print('taskEncoder not broken')
        if ticks_diff(currentTime,next_time) >= 0:
            
            # Move from initialization state to S1_Update state
            if state == S0_INIT:
                state = S1_Update
                
            # Program will run here most of the time
            # Updates encoder position
            elif state == S1_Update:
                encoder1.update()
                timDATA.write(ticks_ms()-init_time)
                DATA.write((timDATA.read(),encoder1.get_position()))
                posDATA.write(encoder1.get_position())
                delDATA.write(encoder1.get_delta())
                speedDATA.write(encoder1.get_delta()/(currentTime-prevTime))
                if zFlag.read():
                    state = S2_Zero
                else:
                    pass
                    
            elif state == S2_Zero:
                encoder1.zero()
                zFlag.write(False)
                state = S1_Update
                
            else:
                raise ValueError (f'Invalid State in {taskName}')
                
            next_time = ticks_add(next_time,period)
            prevTime = currentTime
            
            yield state
        else:
            yield None