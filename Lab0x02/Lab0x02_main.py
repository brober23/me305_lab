"""!@file Lab0x02_main.py
    @brief Main script of ME305 Lab0x02. 
    @details This file instantiates the shared flag variables, shared data variables, 
    and controls the task sharing between taskEncoder.py and taskUser.py.
    @author Logan Williamson
    @author Brianna Roberts
    @date 01/27/2022
"""

import shares, Lab0x02_taskUser, Lab0x02_taskEncoder, Lab0x02_taskMotor

##  @brief Create object zFlag shared between all files.
#   @details Used as a flag between files to communicate when z or Z is pressed. 
#   also used in order to communicate between tasks.
zFlag = shares.Share(False)

##  @brief Create object faultFlag shared between all files.
#   @details Used as a flag between files to communicate when a fault related
#   interrupt request is detected. Used in order to communicate between tasks taskUser.py
#   and taskMotor.py to prompt the user to clear the fault and re-enable the motor.
faultFlag = shares.Share(False)

##  @brief Create an object POS shared between all files.
#   @details This is the value of position that is written in the taskEncoder 
#   and printed in the taskUser when prompted.
DATA = shares.Share(0)   # might want this to be a tuple to share time and position? 

##  @brief Create an object DEL shared between all files.
#   @details This is the value of delta that is written in the taskEncoder 
#   and printed in the taskUser when prompted
DEL = shares.Share(0)

##  @brief Create an object timDATA shared between all files.
#   @details This is the value of time that is written in the taskEncoder 
#   and indexed into the DATA array in taskUser when the user initiates the 
#   data recording functionality of taskUser.
timDATA = shares.Share(0)

##  @brief Create an object posDATA shared between all files.
#   @details This is the value of position that is written in the taskEncoder 
#   and indexed into the DATA array in taskUser when the user initiates the 
#   data recording functionality of taskUser.
posDATA = shares.Share(0)

##  @brief Create an object speedDATA shared between all files.
#   @details This is the value of velocity that is written in the taskEncoder 
#   and can be printed upon pressing 'v' or 'V' by taskUser.py.
speedDATA = shares.Share(0)

##  @brief Create an object delDATA shared between all files.
#   @details This is the value of encoder velocity that is written in the taskEncoder.
delDATA = shares.Share(0)

##  @brief Create an object duty1 shared between all files.
#   @details This object is the duty cycle for the motor1 object instantiated in taskMotor.py
duty1 = shares.Share(0)

##  @brief Create an object duty2 shared between all files.
#   @details This object is the duty cycle for the motor1 object instantiated in taskMotor.py
duty2 = shares.Share(0)

if __name__ == '__main__':
    
    taskList = [Lab0x02_taskUser.taskUserFcn ('Task User', 10_000, zFlag, timDATA, posDATA, delDATA, speedDATA, DATA),
                Lab0x02_taskEncoder.taskEncoderFcn ('Task Encoder', 10_000, zFlag, timDATA, posDATA, speedDATA, delDATA, DATA),
                Lab0x02_taskMotor.taskMotorFcn('Task Motor', 10_000, faultFlag, duty1, duty2)]
    
    while True: 
        try:
            for task in taskList:
                next(task)
                
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')