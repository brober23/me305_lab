'''!@file               Lab0x04_page.py
    @brief              Lab0x04 documentation page.
    @details            This page documents the Lab0x04.py file, which now has a closed loop controller for the motor driver.

	@page page6 		Lab 4 Deliverables
                        *Please see the files tab for file documentation
                        
    @section sec_intro6 Introduction
                        This program consists of five files which work together to generate an encoder object from the encoder.py class file, record the time and position data associated 
                        with that object iteratively in the taskEncoder.py file, and print the results to the console in the taskUser.py file. This last file, taskUser.py, also serves to
                        handle user inputs which reset the encoder position, print the current position and delta (speed) of the encoder, and record position data for 30 seconds or less.
                     
    @section sec_TSK6	Lab 4 Task Diagram
    
                        Task diagram between taskUser and taskEncoder
                        
    @image html         taskDiagram_4.PNG
                            
	@section sec_FSM6	Lab 4 FSM Diagrams
    @image html Lab0x04_taskUser_FSM.jpeg "Lab0x04 updated taskUser.py Finite State Machine"

    @image html Lab0x04_taskMotor_FSM.jpeg "Lab0x04 updated taskMotor.py Finite State Machine"

    @image html taskEncoder_FSM.PNG "Lab0x04 taskEncoder.py Finite State Machine"

    @section sec_data6  Lab 4 Figures
    @image html Lab0x04_StepPlot_1.png "Step Response Plot 1: Maximum Reference Velocity, Small Gain"

    @image html Lab0x04_StepPlot_2.png "Step Response Plot 2: Maximum Reference Velocity, Medium Gain"

    @image html Lab0x04_StepPlot_3.png "Step Response Plot 3: Maximum Reference Velocity, Large Gain"
                        
    @image html Lab0x04_StepPlot_4.png "Step Response Plot 4: Medium Reference Velocity, Medium Gain"

    @image html Lab0x04_StepPlot_5.png "Step Response Plot 5: Maximum Reference Velocity, Medium Gain"
        
    @section sec_src6	Lab 4 source code
						The source code for Lab 4 can be found at https://bitbucket.org/brober23/me305_lab/src/master/lab04/
	@section sec_vid6	A video demonstrating the proper functionality of this program can be seen here:
	\htmlonly
	<iframe title="vimeo-player" src="https://player.vimeo.com/video/682359254?h=80685dd887" width="640" height="360" frameborder="0" allowfullscreen></iframe>
<p><a href="https://vimeo.com/682359254">ClosedLoopMotor</a> from <a href="https://vimeo.com/682359254">Brianna Roberts</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

	\endhtmlonly
	
    @author              Logan Williamson
    @author              Brianna Roberts
    @date                Feburary 02, 2022
'''