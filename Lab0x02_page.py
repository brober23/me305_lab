'''!@file               Lab0x02_page.py
    @brief              Lab0x02 documentation page.
    @details            This page documents the Lab0x02.py file, which outputs and reads data from quadrature encoders.

	@page page2			Lab 2 Deliverables
                        *Please see the files tab for file documentation
                        
    @section sec_intro2 Introduction
                        This program consists of five files which work together to generate an encoder object from the encoder.py class file, record the time and position data associated 
                        with that object iteratively in the taskEncoder.py file, and print the results to the console in the taskUser.py file. This last file, taskUser.py, also serves to
                        handle user inputs which reset the encoder position, print the current position and delta (speed) of the encoder, and record position data for 30 seconds or less.
	@section sec_TSK2	Lab 2 Task Diagram
                        Task diagram between taskUser and taskEncoder
    @image html         taskDiagram.PNG
    @section sec_FSM2	Lab 2 Finite State Machines
                        taskEncoder Finite State Machine
    @image html         taskEncoder_FSM.PNG
                        taskUser Finite State Machine
    @image html         taskUser_FSM.PNG
    @section sec_src2	Lab 2 source code
						The source code for Lab 2 can be found at https://bitbucket.org/brober23/me305_lab/src/master/lab02//
	@section sec_vid2	A video demonstrating the proper functionality of this program can be seen here:
	\htmlonly
	<iframe src="https://player.vimeo.com/video/673017591?h=954c22791e" width="640" height="1138" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/673017591">quadEncoder</a> from <a href="https://vimeo.com/user163567933">Logan Williamson</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
	\endhtmlonly
	
    @author              Logan Williamson
    @author              Brianna Roberts
    @date                Feburary 02, 2022
'''