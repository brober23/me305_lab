'''!@file       taskUser.py
    @brief      Implimentation of User Interface task as an FSM
    @details    The task uses the USB VCP (Virtual COM Port) to take character input 
                from the user working at a serial terminal such as PuTTY. It uses 
                this input with Finite State Machine logic to determine which functionality
                should be executed based on that user input. This file works cooperatively
                with taskEncoder.py to allow both files' functionality to be executed quasi-
                simultaneously.
                
    @author     Brianna Roberts
    @author     Logan Williamson
    @date       01/26/2022
'''
import micropython, array, gc
from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP


##  @brief      Creates object S0_INIT connected to constant 0
#   @details    S0_INIT is used as an initialization state
#               it prints the help interface block and sends FSM to state 1 (S1_Wait)
#               it also checks if zFlag is true which will begin manual calibration
S0_INIT = micropython.const(0)

##  @brief      Creates object S1_Wait connected to constant 1
#   @details    S1_Wait is used as an wait state. Used to decode what the user is 
#               inputting. sends user to respective states given specific keyboard inputs
S1_Wait = micropython.const(1)

##  @brief      Creates object S2_zCalibrate connected to constant 2
#   @details    S2_zCalibrate is used as a zeroing state. If user inputs z or Z,
#               they are sent to this state which zero's the encoder from the taskEncoder
#               uses zFlag to communicate between all files
S2_zCalibrate = micropython.const(2)

##  @brief      Creates object S3_pPrintPos connected to constant 3
#   @details    S3_pPrintPos is used as a printing state. If user inputs p or P,
#               they are sent to this state which prints 3 values of the current position 
#               from the touchpanel (theta_x, theta_y, and theta_z)
S3_pPrintPos = micropython.const(3)

##  @brief      Creates object S4_vVelocity connected to constant 4
#   @details    S4_vVelocity is used as a velocity state. If user inputs v or V,
#               they are sent to this state which prints 3 values of the current velocity 
#               from the touchpanel (omega_x, omega_y, and omega_z)
S4_vVelocity = micropython.const(4)

##  @brief      Creates object S5_mDutyCtrl connected to constant 5
#   @details    S5_mDutyCtrl is used to set any and all system parameters which the 
#               user may need to control. If user inputs m, M, k, d, y, or x they are sent to this 
#               state. In this state, the user can input positive or negative numbers, including
#               decimal numbers. This will change the duty cycles for motor 1 and 2, K_p and K_d,
#               desired theta_y and theta_x values respectively. 
S5_mDutyCtrl = micropython.const(5)

##  @brief      Creates object S6_gCollectThirtySec connected to constant 6
#   @details    S6_gCollectThirtySec is used to collect data from encoder
#               continuously for 30 seconds. The values are then printed out all together
#               in an array. If the user decides to end data collection early, they may
#               do this by entering s or S on the keyboard.
S6_gCollectThirtySec = micropython.const(6)

##  @brief      Creates object S7_PrintData connected to constant 7
#   @details    S7_PrintData is used to print the compiled data that has been 
#               collected from both Encoder data or Testing data. 
S7_PrintData = micropython.const(7)

##  @brief      Creates object S8_wToggleCTRmode connected to constant 8
#   @details    S8_wToggleCTRmode state either enables or disables the closed
#               loop control. If it is disabled, the velocity set will be off proportionally 
#               to the duty cycle. When it is enabled, the corrected gain will alter the 
#               duty cycle to be more accurate. 
S8_wToggleCTRmode = micropython.const(8)

def taskUserFcn(taskName, period, zFlag, timDATA, posDATA, calDATA, DATA, speedDATA, duty_y, duty_x, K_pi, K_di, K_po, K_do, setpoint_x,setpoint_y, wFlag, cFlag):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer number of microseconds.
        @param zFlag        Shared flag variable which causes FSM to enter zeroing state.
        @param timDATA      Shared data for the time during a data recording session. This is not currently utilized by 
                            taskUser.py  
        @param posDATA      Shared data for printing the position of the encoder upon 'p' or 'P' press.
                            This data represents the angular position readout from a BNO055.BNO055 object in degrees.
        @param calDATA      Shared data for printing the delta value of the encoder upon 'c' or 'C' press.
        @param DATA         Shared data for data recording upon 'g' or 'G' press.    
        @param speedDATA    Shared data for printing the velocity of the encoder upon 't' or 'T' press.
        @param faultFlag    Shared flag variable for indicating when a fault condition has occurred.
        @param duty_y       Shared data for controlling the duty cycle of the motor_y object, effectively controlling its speed.
        @param duty_x       Shared data for controlling the duty cycle of the motor_x object, effectively controlling its speed.
        @param K_pi         K_pi is the closed loop proportional inner loop gain factor that is
                            input by the user upon 'k' press
        @param K_di         K_di is the closed loop derivative inner loop gain factor that is
                            input by the user upon 'd' press
        @param K_po         K_po is the closed loop proportional outer loop gain factor that is
                            input by the user upon 'K' press
        @param K_do         K_do is the closed loop proportional outer loop gain factor that is
                            input by the user upon 'D' press
        @param setpoint_x   setpoint_x is set at desired x position by user upon press of 'x' or 'X'
        @param setpoint_y   setpoint_y is set at desired y position by user upon press of 'y' or 'Y'
        @param wFlag        Shared flag variable set or reset by the user entering 'w' or 'W' in State 1. This variable is shared 
                            with taskMotor and will determine whether taskMotor is in the open-loop or closed-loop control state.
        @param cFlag        Local flag variable used to check if calibration coefficient file exists.
    '''
    
    ##  @brief      Variable state describes the current state of the taskIMU finite state machine
    #   @details    State is a local variable which controls the active state of the taskIMU and prevents
    #               this task file from entering mutually exclusive operational states in its operation.
    state = S0_INIT
    
    ##  @brief      start_time which starts the clock.
    #   @details    A timestamp, in microseconds, indicating the current time for 
    #               the run.
    start_time = ticks_us()
    
    ##  @brief      next_time sets an appointment time for the next run.
    #   @details    A timestamp, in microseconds, indicating when the next iteration of the
    #               generator must run.
    next_time = ticks_add(start_time, period)
    
    ##  @brief      serport is a (virtual) serial port object.
    #   @details    A (virtual) serial port object used for reading character inputs
    #               during the cooperative operation of our taskUser state machine with its taskEncoder
    #               counterpart.
    serport = USB_VCP()
    
    ##  @brief      numItems tracks the number of data points collected in State 5.
    #   @details    This object represents the number of data points that have been passed
    #               to taskUser from taskEncoder while taskUser is in the data collection state,
    #               S6_gCollectThirtySec. This object is incremented as each data point is passed in
    #               until it is equal to the desired number of data points defined by totItems.
    numItems = 0
    
    ##  @brief      totItems determines how many data points to gather during collection.
    #   @details    This object represents the maximum value that numItems can increment
    #               to as data is being recorded in State 5, S6_gCollectThirtySec. This corresponds
    #               to the hundredths of a second; in other words, totItems = 3001 will dictate a 
    #               data collection period of 30 seconds, with one data point every 0.01 seconds.
    totItems = 1000

    ## May want these back for plot generation in the future
    ##  @brief    numPrint tracks the number of data points printed in State 6.
    #   @details  This object represents the number of data points that have been
    #             printed while in State 6, S6_PrintData. Once this value reaches the value
    #             of numItems, all the data collected has been printed and the FSM transitions
    #             back to State 1, S1_Wait. This condition also resets numItems and clears the
    #             timeArray and x_array in preparation for the next instance of data collection.
    numPrint = 0
    
    ##  @brief      dutyLim controls the magnitude of the possible closed-loop control 
    #               duty values availabe to the user.
    #   @details    dutyLim serves to control the magnitude of the maximum and minimum duty cycle, 
    #               percentage, that is available for the user to input for closed-loop control. If the user
    #               inputs a duty value that is greater than or equal to dutyLim, their input will be reset to
    #               the dutyLim value to maintain closed-loop controller functionality. If the user inputs a 
    #               duty value that is less than or equal to the negative of the dutyLim value, the duty 
    #               input will similarly be reset to the negative of the dutyLim value.
    dutyLim = 40
    
    ##  @brief      setpointLim controls the magnitude of the possible closed-loop control 
    #               setpoint values availabe to the user.
    #   @details    setpointLim serves to control the magnitude of the maximum and minimum setpoint position, 
    #               in millimeters, that is available for the user to input for closed-loop control. If the user
    #               inputs a setpoint value that is greater than or equal to setpointLim, their input will be reset to
    #               the setpointLim value to maintain closed-loop controller functionality. If the user inputs a 
    #               setpoint value that is less than or equal to the negative of the setpointLim value, the setpoint 
    #               input will similarly be reset to the negative of the setpointLim value.
    setpointLim = 80
    
    ##  @brief      Kp_lowerLim sets a lower limit on acceptable user input values for the closed-loop control gain.
    #   @details    Kp_lowerLim serves to set the lower bound for gain values that will work with the closed-loop 
    #               controller contained in the closedLoop.py class and instantiated in the taskMotor.py task file. Any 
    #               user input for the controller gain that is less than or equal to this lower limit will be replaced with 
    #               this lower limit value to maintain functionality of the closed-loop controller.
    Kp_lowerLim = 0.01
    
    ##  @brief      Kp_upperLim sets an upper limit on acceptable user input values for the closed-loop control gain.
    #   @details    Kp_upperLim serves to set the upper bound for gain values that will work with the closed-loop 
    #               controller contained in the closedLoop.py class and instantiated in the taskMotor.py task file. Any 
    #               user input for the controller gain that is greater than or equal to this upper limit will be replaced with 
    #               this upper limit value to maintain functionality of the closed-loop controller.
    Kp_upperLim = 30
    
    # Data storage arrays for testing and printing states
 
    # NOTE FOR DATA READ RECORDING:
        #going to ADD time data at the END of DATA.
        # that way it's already a shares, and it doesn't affect the indexing of 
        # pre-existing code that uses DATA
    
    ##  @brief      x_array is the array in which collected encoder position data is stored.
    #   @details    This object is an array which will store the encoder position 
    #               data generated during data collection in State 6, S6_gCollectThirtySec.
    #               This data is indexed from the shared parameter, DATA.
    x_array = array.array('f',totItems*[0])
    gc.collect()
    
    
    ##  @brief      x_array is the array in which collected encoder position data is stored.
    #   @details    This object is an array which will store the encoder position 
    #               data generated during data collection in State 6, S6_gCollectThirtySec.
    #               This data is indexed from the shared parameter, DATA.
    y_array = array.array('f',x_array)
    gc.collect()
    
    
    ##  @brief      vx_array is the array in which collected motor velocity data is stored.
    #   @details    This object is an array which will store the velocity data generated during
    #               data collection in State 6, S6_gCollectThirtySec. This data is indexed from the shared
    #               parameter, DATA.
    vx_array = array.array('f',x_array)
    gc.collect()
    
    ##  @brief      vy_array is the array in which collected motor velocity data is stored.
    #   @details    This object is an array which will store the velocity data generated during
    #               data collection in State 6, S6_gCollectThirtySec. This data is indexed from the shared
    #               parameter, DATA.
    vy_array = array.array('f',x_array)
    gc.collect()
    
    # might want to use this for data collection. 
    ##  @brief      timeArray is the array in which collected time data is stored.
    #   @details    This object is an array which will store the time data generated 
    #               during data collection in State 6, S6_gCollectThirtySec. This data is indexed
    #               from the shared parameter, DATA.
    timeArray = array.array('f',x_array)
    gc.collect()

    ##  @brief      Creating local mFlag variable used to simplify state diagram.
    #   @details    mFlag is NOT a shared variable. It is used to communicate 
    #               between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #               cycle. When this flag is raised, it will change duty cycle of motor 1.
    mFlag = False
    
    ##  @brief      Creating local MFlag variable used to simplify state diagram.
    #   @details    MFlag is NOT a shared variable. It is used to communicate 
    #               between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #               cycle. When this flag is raised, it will change duty cycle of motor 2.
    MFlag = False
    
    ##  @brief      Creating local kiFlag variable used to indicate that inner loop controller 
    #               gain is being set.
    #   @details    The kiFlag variable is NOT a shares variable. It acts locally to reserve the 
    #               character buffer for inputs related to setting the inner loop controller 
    #               gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    kiFlag = False
    
    ##  @brief      Creating local koFlag variable used to indicate that closed-loop controller 
    #               gain is being set.
    #   @details    The koFlag variable is NOT a shares variable. It acts to reserve the 
    #               character buffer for inputs related to setting the outer loop controller 
    #               gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    koFlag = False
    
    ##  @brief      Creating local diFlag variable used to indicate that inner loop controller 
    #               derivative gain is being set.
    #   @details    The diFlag variable is NOT a shares variable. It acts to reserve the 
    #               character buffer for inputs related to setting the inner loop controller derivative
    #               gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    diFlag = False
    
    ##  @brief      Creating local doFlag variable used to indicate that outer loop controller 
    #               derivative gain is being set.
    #   @details    The doFlag variable is NOT a shares variable. It acts to reserve the 
    #               character buffer for inputs related to setting the outer loop controller derivative
    #               gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    doFlag = False
      
    ##  @brief      Creating local xFlag variable used to indicate that closed-loop controller 
    #               setpoint is being set.
    #   @details    The xFlag variable is NOT a shares variable. It acts to reserve the 
    #               character buffer for inputs related to setting the closed-loop controller 
    #               gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    xFlag = False  
    
    ##  @brief      Creating local yFlag variable used to indicate that closed-loop controller 
    #               setpoint is being set.
    #   @details    The yFlag variable is NOT a shares variable. It acts to reserve the 
    #               character buffer for inputs related to setting the closed-loop controller 
    #               gain in State 6 S6_mDutyCtrl rather than other values that are also set in this state.
    yFlag = False

    ## Might want to reallocate as IMU_DataFlag for printing in the future
    ##  @brief    Creating IMU_DataFlag variable used to simplify state diagram.
    #   @details  IMU_DataFlag is NOT a shared variable. It is used to communicate 
    #             between S1_Wait and  S7_PrintData.  This flag causes print state to print 
    #             encoder data from S8_gCollectThirtySec after user manually quits using
    #             s or S or after 30 seconds of data collection is up. 
    IMU_DataFlag = False
    
    ##  @brief    Create testDataFlag variable used to simplify state diagram.
    #   @details  testDataFlag is NOT a shared variable. It is used to communicate 
    #             between S1_Wait and  S7_PrintData.  This flag causes print state to print 
    #             Duty data from testDataFlag after user manually quits using
    #             s or S or after 30 seconds of data collection is up. 
    testDataFlag = False
    
    ##  @brief      String for buffering of characters in the serial port of the STM32 Nucleo.
    #   @details    numstr serves to act as a buffer into which characters are written when
    #               when the user is in one of the many parameter input states. This buffer maintains 
    #               characters until the user is finished with an input (presses enter) to allow for 
    #               cooperatively multitasking with taskEncoder and taskMotor to continue running in 
    #               the background.
    numstr = ''
    
    # The finite state machine must run indefinitely.
    while True:
        ##  @brief      current_time tracks the time in microseconds.
        #   @details    This object tracks the elapsed time since taskUser was first called
        #               in order to manage task sharing between taskUser and taskEncoder. It is used
        #               in conjunction with the designated period parameter to set 'appointments' for 
        #               the next run of taskUser and prevent it from dominating task sharing.
        current_time = ticks_us()
        
        # FSM only needs to run if interval has elapsed. must use ticks_diff() b/c of overflow
        if ticks_diff(current_time,next_time) >= 0:
        
            # reset next_time to appointment for next iteration; need ticks_add for overflow 
            next_time = ticks_add(next_time, period)
            
#state 0 code - Initialization state and printing of User Interface directions

            if state == S0_INIT:
                # On the entry to state 0 we can print a message for the user describing the
                # UI.
                print("+----------+----------------------------------------------------------------------------+")
                print('| Command  | Description                                                                |')
                print("+----------+----------------------------------------------------------------------------+")
                print('|  p or P  |  Print platform and touchpanel position data                               |')
                print('|  v or V  |  Print platform and touchpanel velocity data                               |')
                print("+----------+----------------------------------------------------------------------------+")
                print('|    m     |  Enter specified duty cycle for motor 1 (Open Loop control mode)           |')
                print('|    M     |  Enter specified duty cycle for motor 2 (Open Loop control mode)           |')
                print('|  g or G  |  Collect data for 30 seconds                                               |')
                print("+----------+----------------------------------------------------------------------------+")
                print('|  c or C  |  Begin touchpanel calibration process                                      |')
                print('|  w or W  |  Enable or disable closed-loop control                                     |')
                print('|    k     |  Enter new inner loop proportional control gain (Closed Loop control mode) |')
                print('|    d     |  Enter new inner loop derivative control gain (Closed loop control mode)   |')
                print('|    K     |  Enter new outer loop proportional control gain (Closed Loop control mode) |')
                print('|    D     |  Enter new outer loop derivative control gain (Closed Loop control mode)   |')
                print('|  y or Y  |  Choose a theta_y position setpoint                                        |')
                print('|  x or X  |  Choose a theta_x position setpoint                                        |')
                print("+----------+----------------------------------------------------------------------------+")
                print('|  h or H  |  Print this help message                                                   |')
                print('|  Ctrl-C  |  Terminate Program                                                         |')
                print("+----------+----------------------------------------------------------------------------+")
                serport.write('\r')
                if zFlag.read() == True:
                    print('Starting manual calibration. Rotate the platform until all statuses are (3)')
                    state = S2_zCalibrate   # Force calibration of IMU on startup if there is no .txt file for taskIMU to read
                else:
                    print('IMU calibration coefficients file detected. Bypassing manual IMU calibration.')
                    state = S1_Wait
                    
                if cFlag.read() == True:
                    print('Starting manual touchpanel calibration. Touch the indicated positions on the panel.')
                else:
                    print('Touchpanel calibration coefficients file detected. Bypassing manual touchpanel calibration.')
                    
# State 1 code - Base state for awaiting user input
            elif state == S1_Wait:
                # If a character is waiting, read it
                if serport.any():
                    ##  @brief      charIn is the variable where input characters are stored after reading
                    #               and decoding them.
                    #   @details    Reads one character and decodes it into a string. This
                    #               decoding converts the bytes object to a standard Python string object.
                    #               used within state 1
                    charIn = serport.read(1).decode()
                        
                    if charIn in {'p', 'P'}:
                        print('Recieving Position Value')
                        state = S3_pPrintPos # transition to state 3
                        
                    if charIn in {'c', 'C'}:
                        print('Calibrating Touchpanel')
                        cFlag.write(True)
                        state = S1_Wait # transition to state 3
                        
                    elif charIn in {'v', 'V'}:
                        print('Recieving Velocity Value')
                        state = S4_vVelocity # transition to state 5
                    
                    elif charIn == 'm':
                        numstr = ''
                        if wFlag.read() == False:
                            print(f'Please Enter a Duty Cycle between -{dutyLim} and {dutyLim}')
                            mFlag = True
                            state = S5_mDutyCtrl
                        else:
                            print('Must be in open-loop control mode to set duty cycle manually')
                            pass
                        
                    elif charIn == 'M':
                        numstr = ''
                        if wFlag.read() == False:
                            print(f'Please enter a duty cycle between -{dutyLim} and {dutyLim}')
                            MFlag = True
                            state = S5_mDutyCtrl
                        else:
                            print('Must be in open-loop control mode to set duty cycle manually')
                            pass
                        
                    elif charIn in {'g', 'G'}:
                        print(f'Collecting data for {totItems/100} seconds')
                        IMU_DataFlag = True #IMU_DataFlag maybe?
                        state = S6_gCollectThirtySec
                        
                    elif charIn in {'w','W'}:
                        state = S8_wToggleCTRmode

                    elif charIn == 'k':
                        numstr = ''
                        kiFlag = True
                        print(f'Please input an inner loop proportional gain (K_pi) between {Kp_lowerLim} and {Kp_upperLim}:')
                        state = S5_mDutyCtrl
                    
                    elif charIn == 'K':
                        numstr = ''
                        koFlag = True
                        print(f'Please input an outer loop proportional gain (K_po) between {Kp_lowerLim} and {Kp_upperLim}:')
                        state = S5_mDutyCtrl
                        
                    elif charIn == 'd':
                        numstr = ''
                        diFlag = True
                        print(f'Please input an inner loop derivative gain (K_di) between {Kp_lowerLim} and {Kp_upperLim}:')
                        state = S5_mDutyCtrl
                        
                    elif charIn == 'D':
                        numstr = ''
                        doFlag = True
                        print(f'Please input an outer loop derivative gain (K_do) between {Kp_lowerLim} and {Kp_upperLim}:')
                        state = S5_mDutyCtrl
                        
                    elif charIn in {'y','Y'}:
                        numstr = ''
                        print(f'Enter a new closed-loop controller desired y value between -{setpointLim/2} and {setpointLim/2}[mm]')  #does the divide by 2 in fstring cause issues?
                        yFlag = True
                        state = S5_mDutyCtrl
                        
                    elif charIn in {'x','X'}:
                        numstr = ''
                        print(f'Enter a new closed-loop controller desired x value between -{setpointLim} and {setpointLim}[mm]')
                        xFlag = True
                        state = S5_mDutyCtrl
                    
                    elif charIn in {'h', 'H'}:
                        print("+----------+----------------------------------------------------------------------------+")
                        print('| Command  | Description                                                                |')
                        print("+----------+----------------------------------------------------------------------------+")
                        print('|  p or P  |  Print platform and touchpanel position data                               |')
                        print('|  v or V  |  Print platform and touchpanel velocity data                               |')
                        print("+----------+----------------------------------------------------------------------------+")
                        print('|    m     |  Enter specified duty cycle for motor 1 (Open Loop control mode)           |')
                        print('|    M     |  Enter specified duty cycle for motor 2 (Open Loop control mode)           |')
                        print('|  g or G  |  Collect data for 30 seconds                                               |')
                        print("+----------+----------------------------------------------------------------------------+")
                        print('|  c or C  |  Begin touchpanel calibration process                                      |')
                        print('|  w or W  |  Enable or disable closed-loop control                                     |')
                        print('|    k     |  Enter new inner loop proportional control gain (Closed Loop control mode) |')
                        print('|    d     |  Enter new inner loop derivative control gain (Closed loop control mode)   |')
                        print('|    K     |  Enter new outer loop proportional control gain (Closed Loop control mode) |')
                        print('|    D     |  Enter new outer loop derivative control gain (Closed Loop control mode)   |')
                        print('|  y or Y  |  Choose a theta_y position setpoint                                        |')
                        print('|  x or X  |  Choose a theta_x position setpoint                                        |')
                        print("+----------+----------------------------------------------------------------------------+")
                        print('|  h or H  |  Print this help message                                                   |')
                        print('|  Ctrl-C  |  Terminate Program                                                         |')
                        print("+----------+----------------------------------------------------------------------------+")
                        state = S1_Wait
                        
                    elif charIn[0] == 3: # checks for ctrl-c
                        print('Terminating program')
                    else:
                        print(f'You typed invalid {charIn} from state {state}.')
            
# State 2 code - Calibrate IMU state
            elif state == S2_zCalibrate:
                print('mag_stat:',calDATA.read()[0],'acc_stat:',calDATA.read()[1],'gyr_stat:',calDATA.read()[2],'sys_stat:',calDATA.read()[3])
                if not zFlag.read():
                    print('IMU CALIBRATION IS COMPLETE')
                    state = S1_Wait

# State 3 code - Position print state
            elif state == S3_pPrintPos:
                print('Theta_x:',posDATA.read()[0],'Theta_y:',posDATA.read()[1],'Theta_z:',posDATA.read()[2]) #prints platform position values
                print('x-position:',DATA.read()[0],'y-position:',DATA.read()[1],'est. x-velocity:',DATA.read()[2],'est. y-velocity:',DATA.read()[3])
                state = S1_Wait

# State 4 code - Instantaneous velocity print state
            elif state == S4_vVelocity:
                print('Omega_x:',speedDATA.read()[0],'Omega_y:',speedDATA.read()[1],'Omega_z:', speedDATA.read()[2]) # prints velocity value
                state = S1_Wait

# State 5 code - Set motor duty cycle(s) state
        # Wait for character inputs and append them to buffer string
            elif state == S5_mDutyCtrl:
                
                if serport.any():
                    ##  @brief      charDuty is the variable where input characters are stored after reading
                    #               and decoding them.
                    #   @details    Reads one character and decodes it into a string. This
                    #               decoding converts the bytes object to a standard Python string object.
                    #               used for the charDuty input
                    charDuty = serport.read(1).decode()
                    if charDuty.isdigit():
                        numstr += charDuty # everytime charIn is looped appends to string
                        serport.write(charDuty)
                    elif charDuty == '-':
                        if len(numstr) == 0:
                            numstr += charDuty
                            serport.write(charDuty)
                        else:
                            pass
                    elif charDuty in {'\b', '\x08', '\x7f'}: # for backspace
                        if len(numstr)>0:
                            numstr = numstr[:-1]
                            serport.write(charDuty)
                        else:
                            pass
                    elif charDuty == ' ':
                        pass
                    elif charDuty == '.':
                        if numstr.count('.') == 0:
                            numstr += charDuty
                            serport.write(charDuty)
                        else:
                            pass
                    elif charDuty in {'s','S'}:
                        if testDataFlag:
                            print('---------------Data Collection Results---------------')
                            # now going to change to a timestamp and positional values
                            print('          Duty Cycle [%],Velocity [rad/s]            ')
                        state = S7_PrintData
                    elif charDuty in {'\r', '\n'}:
                    # Set duty cycle for open loop control
                        serport.write('\r')
                        if mFlag | MFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= dutyLim:
                                    numstr = dutyLim
                                elif float(numstr) <= -dutyLim:
                                    numstr = -dutyLim
                                numstr = round(float(numstr),2)
                                if mFlag == True:
                                    duty_y.write(numstr)
                                    print('Duty Cycle for Motor 1 has been set to', duty_y.read())
                                    mFlag = False
                                    yield duty_y
                                    state = S1_Wait
                                elif MFlag == True:
                                    duty_x.write(numstr)
                                    print('Duty Cycle for Motor 2 has been set to', duty_x.read())
                                    MFlag = False
                                    yield duty_x
                                    state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between -{dutyLim} and {dutyLim}, then press Enter')
                    # Set proportional gain for inner loop control
                        elif kiFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= Kp_upperLim:
                                    numstr = Kp_upperLim
                                elif float(numstr) <= Kp_lowerLim:
                                    numstr = Kp_lowerLim
                                numstr = round(float(numstr),2)
                                kiFlag = False
                                K_pi.write(numstr)
                                print('Inner loop proportional gain has been set to', K_pi.read())
                                yield K_pi
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between {Kp_lowerLim} and {Kp_upperLim}, then press Enter')
                                state = S5_mDutyCtrl
                    # Set proportional gain for outer loop control
                        elif koFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= Kp_upperLim:
                                    numstr = Kp_upperLim
                                elif float(numstr) <= Kp_lowerLim:
                                    numstr = Kp_lowerLim
                                numstr = round(float(numstr),2)
                                koFlag = False
                                K_po.write(numstr)
                                print('Outer loop proportional gain has been set to', K_po.read())
                                yield K_po
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between {Kp_lowerLim} and {Kp_upperLim}, then press Enter')
                                state = S5_mDutyCtrl
                    # Set gain for inner loop derivative control
                        elif diFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= Kp_upperLim:
                                    numstr = Kp_upperLim
                                elif float(numstr) <= Kp_lowerLim:
                                    numstr = Kp_lowerLim
                                numstr = round(float(numstr),2)
                                diFlag = False
                                K_di.write(numstr)
                                serport.write('\r')
                                print('Inner loop derivative gain has been set to', K_di.read())
                                yield K_di
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between {Kp_lowerLim} and {Kp_upperLim}, then press Enter')
                                state = S5_mDutyCtrl
                     # Set gain for outer loop derivative control
                        elif doFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= Kp_upperLim:
                                    numstr = Kp_upperLim
                                elif float(numstr) <= Kp_lowerLim:
                                    numstr = Kp_lowerLim
                                numstr = round(float(numstr),2)
                                doFlag = False
                                K_do.write(numstr)
                                serport.write('\r')
                                print('Outer loop derivative gain has been set to', K_do.read())
                                yield K_do
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between {Kp_lowerLim} and {Kp_upperLim}, then press Enter')
                                state = S5_mDutyCtrl
                    # Set setpoint for CL control
                        elif yFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= setpointLim/2:
                                    numstr = setpointLim/2
                                elif float(numstr) <= -setpointLim/2:
                                    numstr = -setpointLim/2
                                numstr = round(float(numstr),2)
                                setpoint_y.write(numstr)
                                yield setpoint_y
                                yFlag = False
                                serport.write('\r')
                                print('Closed-loop controller setpoint has been set to', setpoint_y.read())
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between -{setpointLim} and {setpointLim}, then press Enter')
                                state = S5_mDutyCtrl
                        elif xFlag:
                            if len(numstr) > 0:
                                if float(numstr) >= setpointLim:
                                    numstr = setpointLim
                                elif float(numstr) <= -setpointLim:
                                    numstr = -setpointLim
                                numstr = round(float(numstr),2)
                                setpoint_x.write(numstr)
                                yield setpoint_x
                                xFlag = False
                                serport.write('\r')
                                print('Closed-loop controller setpoint has been set to', setpoint_x.read())
                                state = S1_Wait
                            elif len(numstr) == 0:
                                serport.write('\r')
                                print(f'Invalid input. Type a value between -{setpointLim} and {setpointLim}, then press Enter')
                                state = S5_mDutyCtrl
            
## State 8 code - Data Collection State
            elif state == S6_gCollectThirtySec:             
                if numItems < totItems:
                    # might need to create new share to include time stamps?
                   x_array[numItems], y_array[numItems], vx_array[numItems] , vy_array[numItems], timeArray[numItems] = DATA.read()
                   numItems += 1
                   if serport.any():
                       charIn = serport.read(1).decode()
                       if charIn in {'s', 'S'}:
                           print('Ending data collection Early')
                           print('---------------Data Collection Results---------------')
                           print('      Time [s], x-position [mm], y-position [mm], x-velocity [mm/s], y-velocity [mm/s]     ')
                           state = S7_PrintData # transition to state 11
                if numItems == totItems:
                    print('---------------Data Collection Results---------------')
                    print('      Time [s], x-position [mm], y-position [mm], x-velocity [mm/s], y-velocity [mm/s]     ')
                    state = S7_PrintData
                
## State 7 code - Printing State
             # Printing for data collection ('g' press) state
            elif state == S7_PrintData:
                # takes data from touchpanel for printing
                if IMU_DataFlag:
                    if numPrint<numItems:
                        print(f'       {((timeArray[numPrint]-timeArray[0])/1_000_000):.2f}  , {(x_array[numPrint]):.2f}     ,   {(y_array[numPrint]):.2f}  , {(vx_array[numPrint]):.2f}     ,   {(vy_array[numPrint]):.2f} ')
                        numPrint += 1
                    elif numPrint >= numItems:
                        numPrint = 0
                        numItems = 0
                        IMU_DataFlag = False
                        state = S1_Wait
                    
            elif state == S8_wToggleCTRmode:
                if wFlag.read() == False:
                    print('Closed Loop Control Enabled')
                    wFlag.write(True)
                    yield wFlag
                    state = S1_Wait
                elif wFlag.read() == True:
                    wFlag.write(False)
                    print('Closed Loop Control Disabled')
                    yield wFlag
                    state = S1_Wait               

            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            yield state
        else:
            yield None