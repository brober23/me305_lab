"""!@file taskPanel.py
    @brief      Implementation of four wire resistive touchpanel unit calibration and data reading as a finite state machine.
    @details    This file calls the touchpanel.touchpanel class to generate an resistive touchpanel object. 
                This file is called iteratively from the main.py program management file to calibrate and read position data
                from the touchpanel.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       03/08/2022
"""
# import Encoder.py class, other required libraries
from time import ticks_us, ticks_diff, ticks_add, sleep_ms
from ulab import numpy as np
import micropython, touchpanel, os

##  @brief      S0_INIT is the initialization state of the taskPanel.py FSM
#   @details    S0_INIT is used to initialize the FSM. Simply transitions from S0 
#               to S1.
S0_INIT = micropython.const(0)

##  @brief      S1_RUN is the data reading/object updating state of the taskPanel.py FSM
#   @details    S1_RUN is the running state for the taskPanel.py finite state machine. 
#               This state is where the touchpanel object(s) managed by taskPanel.py is/are updated with data
#               read from the physical touchpanel.
S1_RUN = micropython.const(1)

##  @brief      S2_CAL is the calibration state of the taskPanel.py FSM
#   @details    S2_CAL is the calibration state for the taskPanel.py finite state machine. 
#               This state is where the touchpanel object(s) managed by taskPanel.py is/are calibrated upon
#               startup to ensure the data being read from the panel is an accurate reflection of the physical
#               panel surface coordinate system.
S2_CAL = micropython.const(2)

def taskPanelFcn(taskName, period, faultFlag, cFlag, DATA):
    '''!@brief              A generator to implement the touchpanel operation as an FSM.
        @details            The task runs as a generator function and requires a 
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param cFlag        Local flag variable used to check if calibration coefficient file exists.
        @param DATA         Shared data for data recording upon 'g' or 'G' press.
     
    '''
    
# ##  @brief    Creates variable init_time to initiate the clock
# #   @details  Establishes time value when taskBNO055Fcn was first called
# #             This value will start a clock signal when the program is run from main.py.
# init_time = ticks_ms()
    
    ##  @brief      Object next_time calculated the next time for the code iteration
    #   @details    This object adds the current time with the period to get the
    #               next time the code needs to run. It's used for the calculation to tell
    #               the code the respective period has elapsed.
    next_time = ticks_add(ticks_us(),period)
    
    ##  @brief      Object panel represents a physical four wire resistive touchpanel
    #   @details    Object of the touchpanel.touchpanel class. Contains methods for reading the 
    #               position data from a physical four wire resistive touchpanel.
    panel = touchpanel.touchpanel()
    
    ##  @brief      Variable state describes the current state of the taskPanel finite state machine
    #   @details    state is a local variable which controls the active state of the taskPanel and prevents
    #               this task file from simultaneously entering mutually exclusive operation states.
    state = S0_INIT
    
    ##  @brief      Variable for controlling the number of data points collected during calibration
    #   @details    cal_idx is the indexing variable used to control the progression of the calibration
    #               process. This variable indexes by one every time the user touches the panel and generates 
    #               a data point, preparing the program for collection of subsequent data points or processing 
    #               of all collected data.
    cal_idx = 0
    
    ##  @brief      Variable for controlling the number of data points collected during calibration
    #   @details    wait_idx is the indexing variable used to control the progression of the calibration
    #               process. This variable indexes by one every time the taskPanel task runs. By doing so, it prevents 
    #               repeat collection of a certain data point during the panel calibration process.
    wait_idx = 0
    
    printBlocker = False
    
    ##  @brief      Variable filename sets a filename for reading or writing of touchpanel calibration coefficients
    #   @details    filename designates a calibration coefficient filename handle that can be used to check for 
    #               preexisting .txt files containing the calibration coefficients for the touchpanel.
    filename = "touchpanel_cal_coeffs.txt"
    
    # starting alpha and beta values - might want to make shares so taskUser can edit them
    ##  @brief      alpha variable used for position gain alpha beta filtering
    #   @details    Value be must between 0 and 1. The alpha value will be used to calculate 
    #               the predicted x+1 position. alpha gain values help with scaling 
    #               this value. 
    alpha = 0.85
    
    ##  @brief      beta variable used for position gain alpha beta filtering
    #   @details    Value be must between 0 and 1. The beta value will be used to calculate 
    #               the predicted v+1 velocity. beta gain values help with scaling 
    #               this value.
    beta = 0.35
    
    ##  @brief      x_hat variable is the predicted value of x 
    #   @details    x_hat is used to predic the future value of x. it is used in
    #               the calculation by taking the difference of the measured x value
    #               and this value. 
    x_hat = 0
    
    ##  @brief      vx_hat variable is the predicted value of vx 
    #   @details    vx_hat is used to predic the future value of vx. it is used in
    #               the calculation by taking the difference of the measured x value
    #               and this value, then multiplying by the beta value and dividing 
    #               by the time increment 
    vx_hat = 0
    
    ##  @brief      y_hat variable is the predicted value of y 
    #   @details    y_hat is used to predic the future value of y. it is used in
    #               the calculation by taking the difference of the measured y value
    #               and this value. The difference is then multiplied by beta 
    y_hat = 0
    
    ##  @brief      vy_hat variable is the predicted value of vy 
    #   @details    vy_hat is used to predic the future value of vy. it is used in
    #               the calculation by taking the difference of the measured y value
    #               and this value, then multiplying by the beta value and dividing 
    #               by the time increment 
    vy_hat = 0
    
    ##  @brief      Object last_time is used to track the time difference between current and last time
    #   @details    Updated every time the period has elapsed. Used to keep track of the
    #               period iterations.
    last_time = 0
    
    ##  @brief      An empty 5x3 matrix that will be populated with ADC positional values from the panel 
    #   @details    These values will be used to calculate the beta coefficient values of the platform. 
    #               
    ADC_array = np.array([[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]])
    
    beta_coeffs = np.array([[0,0],[0,0],[0,0]])
    
    while True:
        ##  @brief      Object currentTime sets the time clock for this task.
        #   @details    A timestamp, in microseconds, indicating the current clock time
        #               for this class.
        current_time = ticks_us()
        
        # FSM only needs to run if interval has elapsed. must use ticks_diff() b/c of overflow
        if ticks_diff(current_time,next_time) >= 0:
            # reset next_time to appointment for next iteration; need ticks_add for overflow 
            next_time = ticks_add(next_time, period)
            
            ##  @brief      Object T_s tracks the sample time interval between measurements
            #   @details    T_s tracks and records the difference in time between measurements 
            #               for the purpose of alpha-beta filter computations.
            T_s = ticks_diff(current_time, last_time)/1_000_000
            
    ## State 0 code - Initialization state
            if state == S0_INIT:
                if filename in os.listdir():
                    cFlag.write(False)
                    with open(filename, 'r') as f:
                        # Read the first line of the file
                        cal_data_string = f.readline()
                        # Split the line into multiple strings and then convert each one to a float
                    cal_coeffs = cal_data_string.split(',')
                    bufcal = [float(coeff) for coeff in cal_coeffs]
                    beta_coeffs[0,0] = bufcal[0]
                    beta_coeffs[0,1] = bufcal[1]
                    beta_coeffs[1,0] = bufcal[2]
                    beta_coeffs[1,1] = bufcal[3]
                    beta_coeffs[2,0] = bufcal[4]
                    beta_coeffs[2,1] = bufcal[5]
                    state = S1_RUN
                else:
                    cFlag.write(True)
                    state = S2_CAL
                    
    ## State 1 code - Position update state
            elif state == S1_RUN:
                panel.pos_update()
            #Shift and scale ADC readings to reflect [mm] touchpad positions
                panel.x = -(beta_coeffs[0,0]*panel.x_pos+beta_coeffs[0,1]*panel.y_pos+beta_coeffs[2,0])
                panel.y = -(beta_coeffs[1,1]*panel.y_pos+beta_coeffs[1,0]*panel.x_pos+beta_coeffs[2,1])
                panel.z = panel.z_pos
            #Store calibrated touchpanel position data in shares variable
                #DATA.write((panel.x,panel.y))
            #Alpha-Beta filtering
                if panel.z == 1:
                    faultFlag.write(False)
                # filter x-axis data
                    if x_hat == 0:
                        x_hat = panel.x
                    x_hat = x_hat + alpha*(panel.x - x_hat) + T_s*vx_hat
                    vx_hat = vx_hat + (beta/T_s)*(panel.x - x_hat)
                # filter y-axis data
                    if y_hat == 0:
                        y_hat = panel.y
                    y_hat = y_hat + alpha*(panel.y - y_hat) + T_s*vy_hat
                    vy_hat = vy_hat + (beta/T_s)*(panel.y - y_hat)
                    
                elif panel.z == 0:
                    faultFlag.write(True)
                    x_hat = 0
                    y_hat = 0
                    vx_hat = 0
                    vy_hat = 0
                # DATA.write((x_hat,y_hat,vx_hat,vy_hat))
                DATA.write((x_hat,y_hat,vx_hat,vy_hat,current_time))
                last_time = current_time
                
                if cFlag.read() == True:
                    cFlag.write(False)
                    state = S2_CAL
                
    ## State 2 code - Calibration of touchpanel
            elif state == S2_CAL:
                panel.pos_update()
                #print(panel.x_pos,panel.y_pos,panel.z_pos)
                #delay(100)
            ## Calibration procedure
                if cal_idx == 0:
                    if printBlocker == False:
                        print('touch lower left corner of panel to collect first data point')
                        printBlocker = True
                    if panel.z_pos == True:
                        ADC_array[0,0], ADC_array[0,1], ADC_array[0,2] = panel.position
                        cal_idx+=1

                elif cal_idx in {1,3,5,7}:
                    wait_idx+=1
                    if wait_idx == 200:
                        cal_idx += 1
                        wait_idx = 0
                        printBlocker = False
                    else:
                        pass
                elif cal_idx == 2:
                    if printBlocker == False:
                        print('touch lower right corner of panel to collect second data point')
                        printBlocker = True
                    if panel.z_pos == True:
                        ADC_array[1,0], ADC_array[1,1], ADC_array[1,2] = panel.position
                        cal_idx+=1

                elif cal_idx == 4:
                    if printBlocker == False:
                        print('touch upper right corner of panel to collect third data point')
                        printBlocker = True
                    if panel.z_pos == True:
                        ADC_array[2,0], ADC_array[2,1], ADC_array[2,2] = panel.position
                        cal_idx+=1

                elif cal_idx == 6:
                    if printBlocker == False:
                        print('touch upper left corner of panel to collect fourth data point')
                        printBlocker = True
                    if panel.z_pos == True:
                        ADC_array[3,0], ADC_array[3,1], ADC_array[3,2] = panel.position
                        cal_idx+=1

                elif cal_idx == 8:
                    if printBlocker == False:
                        print('touch center of panel to collect final data point')
                        printBlocker = True
                    if panel.z_pos == True:
                        ADC_array[4,0], ADC_array[4,1], ADC_array[4,2] = panel.position
                        cal_idx+=1
                        
                elif cal_idx == 9:
                    # create a y array of known x y locations @ each ADC value
                    y_array = np.array([[80, 40], [-80, 40], [-80, -40], [80, -40], [0, 0]])
                   
                    #Using known positions on the touchpanel and collected ADC
                    #readings, compute coefficients to scale and shift touchapnel outputs
                    ADC_transpose = ADC_array.transpose()
                    ADC_mult = np.dot(ADC_transpose,ADC_array)
                    ADC_invert = np.linalg.inv(ADC_mult)
                    ADCy_mult = np.dot(ADC_transpose,y_array)
                    beta_coeffs = np.dot(ADC_invert,ADCy_mult)
                    
                # Write calibration coefficients to .txt file
                    str_list = [f'{beta_coeffs[0,0]}',f'{beta_coeffs[0,1]}',f'{beta_coeffs[1,0]}',f'{beta_coeffs[1,1]}',f'{beta_coeffs[2,0]}',f'{beta_coeffs[2,1]}']
                    with open(filename, 'w') as f:
                        f.write(','.join(str_list))
                    cal_idx = 0
                    state = S1_RUN
                cFlag.write(False)          
            yield state
        else:
            yield None