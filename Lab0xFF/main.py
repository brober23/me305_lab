"""!@file main.py
    @brief Main script of ME305 Lab0x03. 
    @details This file instantiates the shared flag variables, shared data variables, 
    and controls the task sharing between taskEncoder.py and taskUser.py.
    @author: Logan Williamson
    @author: Brianna Roberts
    @date 01/27/2022
"""

import shares, taskUser, taskMotor, taskIMU, taskPanel

# Flags:
    
##  @brief Create object zFlag shared between all files.
#   @details Used as a flag between files to communicate when z or Z is pressed. 
#   also used in order to communicate between tasks.
zFlag = shares.Share(False)

##  @brief Create object zFlag shared between all files.
#   @details Used as a flag between files to communicate when z or Z is pressed. 
#   also used in order to communicate between tasks.
faultFlag = shares.Share(False)

##  @brief Create object wFlag shared between all files.
#   @details Used as a flag between files to communicate when the program is in closed- 
#   or open-loop control mode. When wFlag is False, the motor speed is being set by 
#   user input (open-loop control). When wFlag is True, the motor speed is set by gain 
#   and setpoint values specified by the user.
wFlag = shares.Share(False)

cFlag = shares.Share(False)

''' Shared Variables from ENCODER '''
##  @brief Create an object timDATA shared between all files
#   @details This is the value of time used in taskEncoder to keep track of
#   time. Set to timDATA.write(ticks_ms()-init_time)
timDATA = shares.Share(0)

##  @brief Create an object posDATA shared between all files
#   @details This is the value of position that is written in the taskEncoder 
#   and printed in the taskUser when prompted
posDATA = shares.Share((0,0,0))

##  @brief Create an object calDATA shared between all files
#   @details This is the parsed calibration status values for printing in taskUser 
#   when attempting to 
#   and printed in the taskUser when prompted
calDATA = shares.Share((0,0,0,0))

##  @brief Create an object DATA shared between all files.
#   @details This is a shares.Share object that records touchpanel position and velocity data.
DATA = shares.Share((0,0,0,0,0))

##  @brief Create an object speedDATA shared between all files
#   @details This is the value of angular velocity that is written in the taskIMU 
#   and printed in the taskUser when prompted
speedDATA = shares.Share((0,0,0))

''' Shared Variables from MOTOR '''
##  @brief Create an object duty1 shared between all files.
#   @details This is a value that will be passed into the motor.Motor objects 
#   for the purpose of setting the duty cycle (and by extension, the speed) of 
#   the
duty_y = shares.Share(0)

##  @brief Create an object DATA shared between all files.
#   @details This is a tuple that records the data and the position. used when 
#   user prompts to ask for 30 second data collection. This is shared with taskUser
#   and formatted to print in an array. 
duty_x = shares.Share(0)

''' Shared Variables from closedLoop '''
##  @brief Create a shared object K_pi corresponding to inner loop controller gain.
#   @details This is a shares.Share object that enables the user to input an inner loop 
#   controller gain.
K_pi = shares.Share(0)

##  @brief Create a shared object K_di corresponding to inner loop controller gain.
#   @details This is a shares.Share object that enables the user to input an inner loop 
#   controller gain.
K_di = shares.Share(0)

##  @brief Create a shared object K_po corresponding to outer loop controller gain.
#   @details This is a shares.Share object that enables the user to input an outer loop 
#   controller gain.
K_po = shares.Share(0)

##  @brief Create a shared object K_do corresponding to outer loop controller gain.
#   @details This is a shares.Share object that enables the user to input an outer loop 
#   controller gain.
K_do = shares.Share(0)

##  @brief Create a shared object setpoint_x corresponding to the desired motor speed.
#   @details This is a shares.Share object that sets the desired motor speed in rad/s. 
#   The values are limited to -175 to 175 rad/s; values outside this range will be 
#   'rounded' to the nearest limit on this range upon input.
setpoint_x = shares.Share(0)

##  @brief Create a shared object setpoint_y corresponding to the desired motor speed.
#   @details This is a shares.Share object that sets the desired motor speed in rad/s. 
#   The values are limited to -175 to 175 rad/s; values outside this range will be 
#   'rounded' to the nearest limit on this range upon input. This controls the motor_1
#   closed loop controller setpoint.
setpoint_y = shares.Share(0)

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    taskList = [taskIMU.taskBNO055Fcn('Task IMU', 10_000, zFlag, posDATA, calDATA, DATA, speedDATA),
                taskPanel.taskPanelFcn('Task Panel',10_000, faultFlag, cFlag, DATA),
                taskUser.taskUserFcn ('Task User', 10_000, zFlag, timDATA, posDATA, calDATA, DATA, speedDATA, duty_y, duty_x, K_pi, K_di, K_po, K_do, setpoint_x, setpoint_y, wFlag, cFlag),
                taskMotor.taskMotorFcn ('Task Motor', 10_000, faultFlag, duty_y, duty_x, K_pi, K_di, K_po, K_do, setpoint_x, setpoint_y, wFlag, posDATA, speedDATA, DATA)]
    
    while True: 
        try:
            for task in taskList:
                next(task)
                
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
        