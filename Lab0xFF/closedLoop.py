"""!@file       closedLoop.py
    @brief      Closed-loop driver module
    @details    This file contains the class ClosedLoop, which implements closed-loop control 
                for a variety of systems and parameters within those systems. The ClosedLoop class is 
                to be used in conjunction with motor and encoder task files to produce the desired 
                closed-loop control behavior.
    @author:    Logan Williamson
    @author:    Brianna Roberts
    @date       02/22/2022
"""

class ClosedLoop:
    '''!@brief      A closed-loop control driver
        @details    Objects of this class can be used to take in input parameters representing 
                    the measured and reference values and return the actuation signal after computing 
                    the controller output.
    '''
    
    
#setpoint is Omega_ref
    def __init__(self, K_p, K_d, setpoint, position, velocity, compensation):
        '''!@brief      Initializes and returns a closedLoop object
            @details    Upon initialization, sets and returns all base objects to 
                        0 and or respective pins. Inputs expected are the proportional
                        control gain, derivative control gain, setpoint, position and velocity. 
                        The position and velocity are initally set as 0 and then updated 
                        iteravely. 
            @param      K_p The closed-loop controller proportional gain
            @param      K_d The closed-loop controller derivative gain
            @param      setpoint The desired speed of the closed loop controller in [rad/s]
            @param      theta parameter representing the measured angular position from a BNO055.BNO055 object.
            @param      omega parameter representing the measured angular speed from a BNO055.BNO055 object.
            @param      xy variable containing coordinate measurements from touchpanel.touchpanel object.
        '''
        ##@brief    K_p is the proportional control gain for a closedLoop.ClosedLoop object
        #@details   K_p represents the proportional closed-loop controller gain associated with a closedLoop.ClosedLoop object
        self.K_p = K_p
        
        ##@brief    K_d is the derivative control gain for a closedLoop.ClosedLoop object
        #@details   K_d represents the derivative closed-loop controller gain associated with a closedLoop.ClosedLoop object
        self.K_d = K_d
        
        ##@brief    pos_ref is the position reference value used in the closed loop control algorithm.  
        #@details   The value of pos_ref is chosen and passed in by the user to produce the desired controller 
        #           output after the controller processes this value with data collected from sensors and passed in via other parameters.
        self.pos_ref = setpoint
        
        ##@brief    pos is the measured/actual position value read by the program for use in the controller
        #@details   The value of pos represents measurements from physical peripherals that are 
        #           passed into the closedLoop.ClosedLoop object for use in the controller.
        self.pos = position
        
        ##@brief    v_ref is the setpoint velocity value read by the program for use in the controller
        #@details   The value of v_ref represents the chosen velocity that a closedLoop.ClosedLoop object should use when
        #           computing the controller output. It is assumed to be zero upon startup, but can be changed depending on the 
        #           desired controller behavior.
        self.v_ref = 0
        
        ##@brief    vel is the measured/actual velocity value read by the program for use in the controller
        #@details   The value of vel represents measurements from physical peripherals that are 
        #           passed into the closedLoop.ClosedLoop object for use in the controller.
        self.vel = velocity
        
        ##@brief    comp is an attribute that stores an adjustment or compensation to be added to PD controller output.
        #@details   The value of comp can be used to store an angle offset or duty cycle compensation factor. Which of 
        #           these values is stored in comp depends on the controller output, but should reflect the units of L.
        self.comp = compensation
        
        ##@brief    L is the calculated duty cycle output of the controller.
        #@details   L represents the duty cycle computed by the closed loop control algorithm to be passed into the 
        #           taskMotor.py task for manipulation of the motor speed when the program is in closed-loop control mode.
        self.L = 0
        
        #self.L = self.K_p*(self.theta_ref - self.theta_meas) + self.K_d*(self.Omega_ref - self.Omega_meas)

    def run(self, K_p, K_d, setpoint, position, velocity, compensation):
        '''!@brief          Updates closed loop controller parameters and runs the controller
            @details        Upon calling the run() method of a closed loop object, sets the passed-in 
                            gains K_p and K_d, updates the setpoint, and uses position and speed data contained in
                            the posDATA and speedDATA parameters to compute a new duty cycle controller output.
            @param K_p      The closed-loop controller proportional gain
            @param K_d      The closed-loop controller derivative gain
            @param setpoint The desired speed of the closed loop controller in [rad/s]
            @param theta    parameter representing the measured angular position from a BNO055.BNO055 object.
            @param omega    parameter representing the measured angular speed from a BNO055.BNO055 object.
            @param xy       variable containing coordinate measurements from touchpanel.touchpanel object.
        '''
        self.K_p = K_p
        self.K_d = K_d
        self.pos_ref = setpoint
        self.pos = position
        self.vel = velocity
        self.comp = compensation
        self.L = self.K_p*(self.pos_ref - self.pos) + self.K_d*(self.v_ref - self.vel) + self.comp
        return self.L
    
if __name__ == '__main__':
    controller = ClosedLoop(0.3,0,0)