"""!@file taskIMU.py
    @brief      Implementation of inertial measurement unit calibration and data reading as a finite state machine.
    @details    This file calls the BNO055.BNO055 class to generate an inertial measurement unit (IMU) object. 
                This file is called iteratively from the main.py program management file to calibrate the IMU. Once 
                calibrated, this file is used to update the output data from the IMU regularly for use in a 
                closed-loop controller file. 
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       01/23/2022
"""
# import Encoder.py class, other required libraries
import BNO055, micropython, os
from time import ticks_us, ticks_add, ticks_diff

##  @brief      creates object S0_INIT connected to constant 0
#   @details    S0_INIT is used to initialize the FSM. Simply transitions from S0 
#               to S1.
S0_INIT = micropython.const(0)

# #  @brief      creates object S1_Update connected to constant 1
#    @details    S1_Update is the running state for the taskIMU.py finite state machine. 
#                This state is where the IMU object(s) managed by taskIMU.py is/are updated with data
#                read from the physical IMU.
S1_Update = micropython.const(1)

# #  @brief      creates object S2_CAL connected to constant 2
#   @details    S2_CAL is the calibration state for the taskIMU.py finite state machine. 
#               This state is where the IMU object(s) managed by taskIMU.py is/are calibrated upon
#               startup to ensure the data being read from the IMU is an accurate reflection of
#               the actual IMU position and dynamics.
S2_CAL = micropython.const(2)

def taskBNO055Fcn(taskName, period, zFlag, posDATA, calDATA, DATA, speedDATA):
    '''!@brief              A generator to implement the IMU task as an FSM.
        @details            The task runs as a generator function and requires a 
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Shared flag variable which causes FSM to enter zeroing state.
        @param posDATA      Shared data array for printing the angular positions of the IMU upon 'p' or 'P' press.
        @param calDATA      Shared data array for printing the position of the encoder upon 'c' or 'C' press.
        @param DATA         Shared data for data recording upon 'g' or 'G' press. This 
        @param speedDATA    Shared data array for printing the angular velocities of the IMU upon 'd' or 'D' press.
    '''
    
# ##  @brief    Creates variable init_time to initiate the clock
# #   @details  Establishes time value when taskBNO055Fcn was first called
# #             This value will start a clock signal when the program is run from main.py.
# init_time = ticks_ms()
    
    ##  @brief      Object next_time sets an appointment time for the next run
    #   @details    A timestamp, in microseconds, indicating when the next iteration of the
    #               generator must run. Uses ticks_add to add current ticks to period.
    next_time = ticks_add(ticks_us(),period)
    
    ##  @brief      Object IMU is the inertial measurement unit object containing platorm dynamic data
    #   @details    IMU is an object of the BNO055.BNO055 class. It contains methods for reading the 
    #               dynamic and position data from a physical Adafruit BNO055 IMU device.
    IMU = BNO055.BNO055()
    
    ##  @brief      Variable state describes the current state of the taskIMU finite state machine
    #   @details    state is a local variable which controls the active state of the taskIMU and prevents
    #               this task file from entering mutually exclusive operational states in its operation.
    state = S0_INIT
    
    ##  @brief      Variable filename sets a filename for generation and writing to 
    #   @details    filename designates a handle for writing or reading of calibration coefficients.
    filename = "IMU_cal_coeffs.txt"
    
    while True:
        ##  @brief      Object currentTime sets the time clock for this task.
        #   @details    A timestamp, in microseconds, indicating the current clock time
        #               for this class.
        current_time = ticks_us()
        
        # FSM only needs to run if interval has elapsed. must use ticks_diff() b/c of overflow
        if ticks_diff(current_time,next_time) >= 0:
        
            # reset next_time to appointment for next iteration; need ticks_add for overflow 
            next_time = ticks_add(next_time, period)
            
        ## Initialization state
            if state == S0_INIT:
                if filename in os.listdir():   # if file exists, read from it
                    # open filename and 'r' read from it
                    with open(filename, 'r') as f:
                        # Read the first line of the file
                        ##  @brief      cal_data_string string object set to read the file lines 
                        #   @details    cal_data_string uses readline to iteravely read lines consecutively.
                        cal_data_string = f.readline()
                        # Split the line into multiple strings and then convert each one to a float
                    ##  @brief      cal_coeffs splits cal_data_string into individual floats
                    #   @details    cal_coeffs uses split to split the line into multiple 
                    #               strings and then convert each one to a float
                    cal_coeffs = cal_data_string.split(',')
                    for i in range(0, len(cal_coeffs)):
                        cal_coeffs[i] = int(cal_coeffs[i],16)
                        IMU.bufcal[i] = cal_coeffs[i]
                    IMU.set_mode(0)
                    IMU.write_calConstants(IMU.bufcal)
                    IMU.set_mode(2)
                    state = S1_Update
                else:
                    zFlag.write(True)
                    state = S2_CAL
                
        ## Update state - where the FSM will be most of the time, where data is read from IMU
            elif state == S1_Update:
                IMU.update()
                IMU.get_data()
                posDATA.write((IMU.theta_x, IMU.theta_y, IMU.theta_z))
                speedDATA.write((IMU.omega_x, IMU.omega_y, IMU.omega_z))
                
                if zFlag.read() == True:
                    state = S2_CAL
                    
        ## Calibration state - where the FSM will go when the user wants to calibrate the device
            elif state == S2_CAL:
                IMU.update()
                # if filename in os.listdir():   # if file exists, read from it
                #     # open filename and 'r' read from it
                #     with open(filename, 'r') as f:
                #         # Read the first line of the file
                #         cal_data_string = f.readline()
                #         # Split the line into multiple strings and then convert each one to a float
                #     cal_coeffs = cal_data_string.split(',')
                #     for i in range(0, len(cal_coeffs)):
                #         cal_coeffs[i] = int(cal_coeffs[i],16)
                #         IMU.bufcal[i] = cal_coeffs[i]
                #     IMU.set_mode(0)
                #     IMU.write_calConstants(IMU.bufcal)
                #     IMU.set_mode(2)
                #     zFlag.write(False)
                #else:
                IMU.set_mode(2)
                IMU.get_calStatus()
                calDATA.write((IMU.mag_stat,IMU.acc_stat,IMU.gyr_stat,IMU.sys_stat))
                yield calDATA
                if IMU.mag_stat==3 and IMU.acc_stat==3 and IMU.gyr_stat==3 and IMU.sys_stat==3:
                    IMU.get_calConstants()
                    cal_coeffs = IMU.cal_coeffs
                    str_list = []
                    for cal_coeff in cal_coeffs:
                        str_list.append(hex(cal_coeff))
                    with open(filename, 'w') as f:
                        f.write(','.join(str_list))
                        zFlag.write(False)
                        state = S1_Update
                
            else:
                raise ValueError (f'Invalid State in {taskName}')
            yield state
        else:
            yield None
            
            
# if __name__ == '__main__':
#     # Adjust the following code to write a test program for your motor class. Any
#     # code within the if __name__ == '__main__' block will only run when the
#     # script is executed as a standalone program. If the script is imported as
#     # a module the code block will not run.
#     bus_addr = 0x28                       #Physical bus address for i2c
#     IMU = BNO055(0)
#     IMU.set_mode(2)
    
# # Testing for proper positional output data

#     while True:
#         IMU.update()
#         IMU.get_calStatus()
#         #print(IMU.theta_x/16,IMU.theta_y/16,IMU.theta_z/16)
#         print(IMU.mag_stat, IMU.acc_stat, IMU.gyr_stat, IMU.sys_stat)
#         sleep_ms(1000)
        
        
# # Testing for proper positional output data
#     while True:
#         IMU.update()
#         IMU.get_data()
#         #print(IMU.theta_x/16,IMU.theta_y/16,IMU.theta_z/16)
#         print(IMU.omega_x/16,IMU.omega_y/16,IMU.omega_z/16)
#         sleep_ms(1000)