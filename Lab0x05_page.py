'''!@file               Lab0x05_page.py
    @brief              Lab0x05 documentation page.
    @details            This page documents the Lab0x05.py file, which now has a BNO055 driver for the two motors and platform balancing.

	@page page7 		Lab 5 Deliverables
                        *Please see the files tab for file documentation
                        
    @section sec_intro7 Introduction
                        This program consists of eight files which work cooperatively (by means of cooperative multitasking via time slicing) to balance a <br>
                        platform about a central pivot by two small permanent magnet DC motors effectively giving it 2 degrees of freedom about the pivot <br>
                        The files include the motor.Motor, closedLoop.ClosedLoop, BNO055.BNO055, and shares.Share classes. <br>
                        Instantiation of objects from these classes, as well as collaborative program functionality, is executed by the taskUser.py user interface file, the taskMotor.py motor control file,<br>
                        and the taskIMU.py inertial measurement unit control file. Documentation of these files can be found in the Files tab of this page.<br>
         
    @section sec_TSK7	Lab 5 Task Diagram
    
    \htmlonly <style>div.image img[src="Lab0x05_taskDiagram.png"]{width:640px;}</style> \endhtmlonly 
    @image html Lab0x05_taskDiagram.png "Task Sharing Diagram for Lab0x05 - Ball Balancing Platform"
    
    @section sec_FSM7	Lab 5 FSM Diagrams
    \htmlonly <style>div.image img[src="Lab0x05_FSM_taskUser.png"]{width:1280px;}</style> \endhtmlonly 
    @image html Lab0x05_FSM_taskUser.png "Lab 0x05 User Interface Finite State Machine"
        
    \htmlonly <style>div.image img[src="Lab0xFF_FSM_taskMotor.png"]{width:919px;}</style> \endhtmlonly 
    @image html Lab0xFF_FSM_taskMotor.png "Lab 0x05 User Interface Finite State Machine"
    
    \htmlonly <style>div.image img[src="Lab0xFF_FSM_taskIMU.png"]{width:928px;}</style> \endhtmlonly 
    @image html Lab0xFF_FSM_taskIMU.png "Lab 0x05 User Interface Finite State Machine"
        
    @section sec_src7	Lab 5 source code
    					The source code for Lab 5 can be found at https://bitbucket.org/brober23/me305_lab/src/master/Lab0x05/
	@section sec_vid7	A video demonstrating the proper functionality of this program can be seen here:
	\htmlonly
    <iframe src="https://player.vimeo.com/video/689892516?h=0639712a97" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/689892516">ME305_Lab0x05</a> from <a href="https://vimeo.com/user164261385">Brianna Roberts</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
	\endhtmlonly
	
    @author              Logan Williamson
    @author              Brianna Roberts
    @date                March 18, 2022
'''