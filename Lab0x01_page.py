'''!@file               Lab0x01_page.py
    @brief              Lab0x01_page.py documentation page.
    @details            This page documents the Lab0x01.py file, which blinks an LED in one of three available patterns.

	@page page1			Lab 1 Documentation and Deliverables

    @section sec_intro1 Introduction
                        Upon startup, this file assigns the relevant hardware pins for the STM32 Nucleo LED and button to descriptive variables using the micropython library, and enters a waiting state. 
						When the user presses the input button on the board, this transitions the program to State 1, which is a square wave blinking of the LED with period of one second. An additional 
						press of the button will transition the program to State 2, which is a sine wave blinking pattern of the LED with period of ten seconds. A third button press transitions the program
						to State 3, which is a sawtooth wave blinking of the LED with a period of one second. Pressing the button in State 3 will cycle back to State 1 and continue through the cycle as described
						above. This program can be broken out of by pressing ctrl+c.
	@section sec_FSM1	Lab 1 Finite State Machine
	
    @section sec_src1	Lab 1 source code
						The source code for Lab 1 is a file called ME305_Lab0x01.py which can be found at https://bitbucket.org/brober23/me305_lab/src/master/Lab0x01/
	@section sec_vid1	A video demonstrating the proper functionality of this file can be seen here:
	\htmlonly
	<iframe title="vimeo-player" src="https://player.vimeo.com/video/666199651?h=4809318802" width="640" height="360" frameborder="0" allowfullscreen></iframe>
	\endhtmlonly
	
    @author              Logan Williamson
    @author              Brianna Roberts
    @date                January 15, 2022
'''