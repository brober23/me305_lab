'''!@file               Lab0xFF_page.py
    @brief              Lab0xFF documentation page.
    @details            This page documents the ME 305 Introduction to Mechatronics term project. These files comprise a cooperative multitasking program that balances a steel ball on <br>
                        a platform.

	@page page8			Term Project Deliverables
                        *Please see the files tab for file documentation
    @section sec_src8	Lab0xFF
                        The source code files for Lab0xFF can be found at https://bitbucket.org/wokka29/me305_labs/src/master/Lab%200xFF/
    @section sec_intro8 Introduction
                        This program consists of ten files which work cooperatively (by means of cooperative multitasking via time slicing) to balance a steel ball on a platform. This platform is actuated <br>
                        about a central pivot by two small permanent magnet DC motors. The files include the motor.Motor, closedLoop.ClosedLoop, BNO055.BNO055, touchpanel.touchpanel, and shares.Share classes. <br>
                        Instantiation of objects from these classes, as well as collaborative program functionality, is executed by the taskUser.py user interface file, the taskMotor.py motor control file,<br>
                        the taskIMU.py inertial measurement unit control file, and the taskPanel.py resistive touchpanel control file. Documentation of these files can be found in the Files tab of this page.<br>
	@section sec_FSM8	Term Project Diagrams
    
    \htmlonly <style>div.image img[src="Lab0xFF_taskDiagram.png"]{width:893px;}</style> \endhtmlonly 
    @image html Lab0xFF_taskDiagram.png "Task Sharing Diagram for Lab0xFF - Ball Balancing Platform"
    
\htmlonly <style>div.image img[src="Lab0xFF_FSM_taskUser.png"]{width:1000px;}</style> \endhtmlonly 
@image html Lab0xFF_FSM_taskUser.png "Term Project User Interface Finite State Machine"

\htmlonly <style>div.image img[src="Lab0xFF_FSM_taskPanel.png"]{width:800px;}</style> \endhtmlonly 
@image html Lab0xFF_FSM_taskPanel.png "Term Project Resistive Touchpanel Finite State Machine"

\htmlonly <style>div.image img[src="Lab0xFF_FSM_taskMotor.png"]{width:800px;}</style> \endhtmlonly 
@image html Lab0xFF_FSM_taskMotor.png "Term Project PMDC Motors Finite State Machine"

\htmlonly <style>div.image img[src="Lab0xFF_FSM_taskIMU.png"]{width:800px;}</style> \endhtmlonly 
@image html Lab0xFF_FSM_taskIMU.png "Term Project Inertial Measurement Unit Finite State Machine"

@section sec_data8 Term Project Figures
    
\htmlonly <style>div.image img[src="Lab0xFF_xyPlot.png"]{width:600px;}</style> \endhtmlonly 
@image html Lab0xFF_xyPlot.png "Path of the ball traced out on the touchpanel over 10 seconds of data collection"
    
@section sec_vid8	Demonstration of platform balancing a ball:
\htmlonly
<iframe src="https://player.vimeo.com/video/689844173?h=f7ca334fd5" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/689844173">ME305_FinalProject</a> from <a href="https://vimeo.com/user164261385">Brianna Roberts</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
\endhtmlonly

\htmlonly
<iframe title="vimeo-player" src="https://player.vimeo.com/video/689879556?h=9a704f14e8" width="640" height="354" frameborder="0" allowfullscreen></iframe>
\endhtmlonly

@section sec_bgCalcs
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0001.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0001.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0002.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0002.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0003.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0003.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0004.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0004.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0005.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0005.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0006.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0006.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0007.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0007.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0008.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0008.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0009.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0009.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0010.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0010.jpg
    
    @author              Logan Williamson
    @author              Brianna Roberts
    @date                March 18, 2022
'''