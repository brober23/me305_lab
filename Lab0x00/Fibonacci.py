# -*- coding: utf-8 -*-
"""@file Fibonacci.py

Created on Thu Jan  6 08:57:49 2022
@author: Brianna Roberts
@author: Logan Williamson
"""

def fib(idx):
    """
   Description: This function calculates a Fibonacci number at a specific index.

    Parameters
    ----------
    @idx        An integer specifying the index of the desire Fibonacci number.
    -------
    Returns     Fibonacci number associated with index, idx.
    
    """
    fib_list = (idx+1)*[0]      #Preallocates list of zeros with length of idx
    fib_list[0] = 0             #Sets index 0 to 0
    if idx>0:                   #Sets index 1 to 1 only if idx>0 to avoid exceeding list bounds
        fib_list[1] = 1
    else:
        pass
    
    #This block runs through every list index, starting at index 2 since 0 and 1 are already
    #defined, and computes the each Fibonacci number sequentially, storing the results in the
    #appropriate index in fib_list. It then returns the list item with index idx, i.e. what the
    #user wants the program to output.
    i = 2
    while i<= idx:
        fib_list[i] = fib_list[i-1]+fib_list[i-2]
        i = i+1
    return(fib_list[idx])
      
#Testing code
if __name__ == '__main__':
    #This block establishes a logical condition that will re-prompt the user for 
    #inputs until the quit key is entered as an input. It then prompts the user 
    #for input, validates that the input is a positive integer, calls the fib 
    #function for their input, and prints the result. Entering q rather than an 
    #integer will change the reprompt condition to False and exit the program.
    again = True
    while again == True:
        idx = input('Please enter a positive integer index or press q to quit:')
        if idx.isnumeric():
            idx = int(idx)
            print(fib(idx))
        elif idx == 'q':
            again = False
            
        else:
            print ('Invalid index. Please enter a positive integer index or press q to quit:')
    print('Thanks for using this Fibonacci calculator. Goodbye.')