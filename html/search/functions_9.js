var searchData=
[
  ['sawwave_0',['SawWave',['../_m_e305___lab0x01_8py.html#a0a3c2c00f7d5b73cfe04f64f11fe1351',1,'ME305_Lab0x01']]],
  ['scan_5fx_1',['scan_x',['../classtouchpanel_1_1touchpanel.html#a2a617eeaf86ee77078e263f9da514949',1,'touchpanel::touchpanel']]],
  ['scan_5fy_2',['scan_y',['../classtouchpanel_1_1touchpanel.html#a68fece816577cf0db7942dbbae862a5b',1,'touchpanel::touchpanel']]],
  ['scan_5fz_3',['scan_z',['../classtouchpanel_1_1touchpanel.html#a32b77523b17872adb665dd001024c6bc',1,'touchpanel::touchpanel']]],
  ['set_5fduty_4',['set_duty',['../class_lab0x03__motor_1_1_motor.html#a4d9a99a7f39eed4caaf990db1f6465bd',1,'Lab0x03_motor.Motor.set_duty()'],['../class_lab0x04__motor_1_1_motor.html#a7566e06853ab893aa00a71ca7b426b8b',1,'Lab0x04_motor.Motor.set_duty()'],['../class_lab0x05__motor_1_1_motor.html#ab4d9a677a0a4ae7efccc024b7a10d9ec',1,'Lab0x05_motor.Motor.set_duty()'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty()']]],
  ['set_5fmode_5',['set_mode',['../class_b_n_o055_1_1_b_n_o055.html#a08beba44b8a797f5374845e04b72a7cf',1,'BNO055::BNO055']]],
  ['sinewave_6',['SineWave',['../_m_e305___lab0x01_8py.html#a7836a70015d61589c781780d9ed54b5e',1,'ME305_Lab0x01']]],
  ['squarewave_7',['SquareWave',['../_m_e305___lab0x01_8py.html#ab3ba22ace72e57e02a7b8e2f99b6ccf8',1,'ME305_Lab0x01']]]
];
