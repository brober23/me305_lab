'''!@file                mainpage.py
    @brief               Brief Documentation for portfolio website
    @details             This file serves as the seed file for Doxygen to generate a mainpage for my Bitbucket documentation repository.

    @mainpage
    @section sec_Projects Projects
                         \htmlonly 
                         <a href="https://brober23.bitbucket.io/page1.html">Lab0x01 - Getting Started with Hardware</a>
                         \endhtmlonly
                         
                         \htmlonly
                         <a href="https://brober23.bitbucket.io/page2.html">Lab0x02 - Incremental Encoders</a>
                         \endhtmlonly
                         
                         \htmlonly
                         <a href="https://brober23.bitbucket.io/page3.html">Lab0x03 - PMDC Motors</a>
                         \endhtmlonly
                         
                         \htmlonly
                         <a href="https://brober23.bitbucket.io/page6.html">Lab0x04 - Closed Loop Motor Control</a>
                         \endhtmlonly
                         
                         \htmlonly
                         <a href="https://brober23.bitbucket.io/page7.html">Lab0x05 - Table Balance</a>
                         \endhtmlonly
                         
                         \htmlonly
                         <a href="https://brober23.bitbucket.io/page8.html">Lab0xFF - Final Project</a>
                         \endhtmlonly
                         
    @section sec_Homework Files and Calculations
                         \htmlonly
                         <a href="https://brober23.bitbucket.io/page4.html">Homework0x02 - Ball Balancer System Modeling</a>
                         \endhtmlonly
                         
                         \htmlonly
                         <a href="https://brober23.bitbucket.io/page5.html">Homework0x03 - Ball Balancer Simulation</a>
                         \endhtmlonly
                         
    @section sec_intro   Introduction
                        This documentation repository serves to organize and present my progress throughout the course of 
                        ME 305 - Intro to Mechatronics at California Polytechnic State University San Luis Obispo, CA. 
                        Documentation for each project completed in this course can be found under the 'Files' tab above. 
    @section sec_mot     Motor Driver
                        Some information about the motor driver with links.
                        Please see motor.Motor for details.
    @section sec_enc     Encoder Driver
                        Some information about the encoder driver with links.
                        Please see encoder.Encoder for details.

    @author              Brianna Roberts

    @copyright           Don't you use this code or else....!!!!

    @date                January 15, 2022
'''