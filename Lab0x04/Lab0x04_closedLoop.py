"""!@file Lab0x04_closedLoop.py
    @brief Closed-loop driver module
    @details This file contains the class ClosedLoop, which implements closed-loop control 
    for a variety of systems and parameters within those systems. The ClosedLoop class is 
    to be used in conjunction with motor and encoder task files to produce the desired 
    closed-loop control behavior.
    @author: Logan Williamson
    @author: Brianna Roberts
    @date 02/22/2022
"""

class ClosedLoop:
    '''!@brief A closed-loop control driver
        @details Objects of this class can be used to take in input parameters representing 
        the measured and reference values and return the actuation signal after computing 
        the controller output.
    '''
    
    
#setpoint is Omega_ref
    def __init__(self,K_p,setpoint,speedDATA):
        '''!@brief Initializes and returns a closed-loop controller object.
            @details Upon calling closedLoop.ClosedLoop, instantiates a closed-loop controller object.
            @param K_p The closed-loop controller gain
            @param setpoint The desired speed of the closed loop controller in [rad/s]
            @param speedDATA shares.Share variable representing the measured speed of an encoder object.
        '''
        
        self.Omega_meas = speedDATA
        self.Omega_ref = setpoint
        self.Gain = K_p
        self.L = self.Gain*(self.Omega_ref - self.Omega_meas)
        
    def run(self, K_p, setpoint, speedDATA):
        ##@brief Gain is the local interpretation of the K_p parameter within an object
        #@details Gain represents the closed-loop controller gain associated with a ClosedLoop object
        self.Gain = K_p
        ##@brief Omega_ref is the local interpretation of the setpoint parameter within an object
        #@details Omega_ref represents the closed-loop controller setpoint associated with a ClosedLoop object
        self.Omega_ref = setpoint
        ##@brief Omega_meas is the local interpretation of the speedDATA parameter within an object
        #@details Omega_meas represents the closed-loop controller speedDATA associated with a ClosedLoop object
        self.Omega_meas = speedDATA
        ##@brief L is the calculated duty cycle output of the controller.
        #@details L represents the duty cycle computed by the closed loop control algorithm to be passed into the 
        #taskMotor.py task for manipulation of the motor speed when the program is in closed-loop control mode.
        self.L = self.Gain*(self.Omega_ref - self.Omega_meas)
        
        return self.L
    
if __name__ == '__main__':
    controller = ClosedLoop(0.3,0,0)