"""!@file Lab0x04_DRV8847.py
    @brief A driver for controlling motors
    @details This file contains a class which can be called to generate a motor
    driver object. That object allows for control of any subordinate objects generated
    from a motor class via a single pulse-width modulation channel. This class also allows 
    the user to detect faults from, enable, or disable any subordinate motor objects.
    @author Logan Williamson
    @author Brianna Roberts
    @date 02/09/2022
"""

from Lab0x04_motor import Motor
from pyb import Pin, Timer, ExtInt
from time import sleep_us
import micropython

class DRV8847:
    '''!@brief A motor driver class for the DRV8847 from TI.
        @details Objects of this class can be used to configure the DRV8847
        motor driver and to create one or more objects of the
        Motor class which can be used to perform motor
        control.
        Refer to the DRV8847 datasheet here:
            https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    def __init__ (self,timerPin,sleepPin,faultPin):
        '''!@brief Initializes and returns a DRV8847 object.
            @details Upon calling the DRV8847 class, the __init__ method will instantiate
            a motor driver object with the timerPin, sleepPin, and faultPin pins passed in
            by the user.
        '''
        ##  @brief tim creates new timer object for tracking time data and correlating it with encoder position.
        #   @details Initiates a timer corresponding to the passed in variable timerPin.
        self.tim = Timer(timerPin)
        ##  @brief nSLEEP creates new pin object for enabling and disabling of the DRV8847 driver.
        #   @details Initiates a pin corresponding to the passed in variable sleepPin. This pin corresponds 
        #   to the enable/disable pin for the motor driver.
        self.nSLEEP = Pin(sleepPin, mode=Pin.OUT_PP)
        ##  @brief nFAULT creates new pin object associated with the driver fault pin.
        #   @details Initiates a pin corresponding to the passed in variable faultPin. This pin
        #   will be pulled logic low in the event that a fault condition is detected in either 
        #   motor controlled by the DRV8847 driver.
        self.nFAULT = Pin(faultPin)
        ##  @brief faultInt creates an external interrupt request object.
        #   @details Initiaties an object that represents the external interrupt request which is 
        #   sent to in the event that a fault condition is encountered. This will control the faultPin.
        self.faultInt = ExtInt(self.nFAULT, mode=ExtInt.IRQ_FALLING,
                               pull=Pin.PULL_NONE, callback=self.fault_cb)
        ##  @brief faultStatus is used to indicate the current status of the driver.
        #   @details faultStatus True indicates that a fault has occurred and must be 
        #   cleared for continued operation of the driver. faultStatus False indicates 
        #   that the driver is still operational.
        self.faultStatus = False
        
    def enable (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
            @details The enable method disables the faultInt external interrupt
            and resets the nSLEEP pin specified upon driver instatiation to 'high'
            before waiting 50 microseconds to re-enable faultInt in preparation for
            another potential fault condition occurring. This will allow the user 
            to once again operate the motor driver.
        '''
        self.faultInt.disable()
        self.nSLEEP.high()
        sleep_us(25)
        self.faultInt.enable()

    
    def disable (self):
        '''!@brief Puts the DRV8847 in sleep mode.
            @details The disable method will disable all motors controlled by the motor
            driver class by setting the nSLEEP pin to low.
        '''
        self.nSLEEP.low()
    
    def fault_cb (self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
            @param IRQ_src The source of the interrupt request.
        '''
        self.disable()
        self.faultStatus = True
    
    def motor (self, PWM_tim, IN1_pin, IN2_pin, chA, chB):
        '''!@brief Creates a DC motor object connected to the DRV8847.
            @return An object of class Motor with the specified: pulse-width modulation
            timer channel, input 1 and input 2 pins, and channels 1 and 2.
            @param PWM_tim The pulse-width modulation timer for control of the motor object duty cycle.
            @param IN1_pin The first of two pins required to supply power to a motor.
            @param IN2_pin The second of two pins required to supply power to a motor.
            @param chA One of two channels which control the direction of rotation of a motor
            @param chB One of two channels which control the direction of rotation of a motor.
        '''
        return Motor(PWM_tim, IN1_pin, IN2_pin, chA, chB)

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Fault debugging tool
    micropython.alloc_emergency_exception_buf(100)

    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    motor_drv = DRV8847(3,Pin.cpu.A15,Pin.cpu.B2)
    motor_1 = motor_drv.motor(3, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    motor_2 = motor_drv.motor(3, Pin.cpu.B4, Pin.cpu.B5, 3, 4)

    # Enable the motor driver
    motor_drv.enable()

    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent for immediate testing/showing that this class is working
    motor_1.set_duty(40)
    motor_2.set_duty(60)